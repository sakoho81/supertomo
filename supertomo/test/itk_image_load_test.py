import os
import sys
from iocbio.io import itk_io_utilites as itkutils

image_file_path_prefix = '/home/sami/Pictures/TestImages'

if len(sys.argv) < 2:
    print 'You should give an image file as a parameter'
    sys.exit(1)

image_path = os.path.join(image_file_path_prefix, sys.argv[1])

image = itkutils.itk_read_image(image_path, image_type='IUC3')


print itkutils.itk_get_dimensions(image)