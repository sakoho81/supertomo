

include_tiff_h = '/usr/include/x86_64-linux-gnu/tiff.h'

print "Tiff header file %s" % include_tiff_h

# Read TIFFTAG_* constants for the header file:
f = open(include_tiff_h, 'r')
l = []
d = {}
for line in f.readlines():
    # Only definition lines are of interest
    if not line.startswith('#define'):
        continue

    # Remove #define from beginning
    line = line.strip('#define')

    # Remove comments
    if '/*' in line:
        line = line.partition('/*')[0]

    # Split to name,value pairs
    words = line.split()
    if len(words) != 2:
        continue
    name, value = words

    #Save to dictionary
    if value in d:
        value = d[value]
    else:
        value = eval(value)
    d[name] = value
    l.append('%s = %s' % (name, value))

f.close()