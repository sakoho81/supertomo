import sys
import os
from iocbio.io import itkio
from iocbio.microscope import itkutils, plots

image_file_path_prefix = '/home/sami/Pictures/TestImages'
image_type = 'IUC3'

if len(sys.argv) < 3:
    print "Error: You must specify a data file to visualize, and " \
          "scaling size"
    sys.exit(1)

image_path = os.path.join(image_file_path_prefix, sys.argv[1])
image = itkio.read_image(image_path, image_type)

multiplier = float(sys.argv[2])

if multiplier < 0 or multiplier >= 1:
    print "Error: Multiplier must be between zero and one"
    sys.exit(1)

small_image = itkutils.get_image_subset(image, image_type, multiplier)

plots.plot_3d_volume(small_image)
