__author__ = 'sami'

import sys
import os
from iocbio.io import  itkio
from iocbio.io.image_stack import ImageStack
import warnings

image_file_path_prefix = '/home/sami/Pictures/TestImages'
image_type = 'IUC3'

if len(sys.argv) < 2:
    print 'You should give an image file as a parameter'
    sys.exit(1)

image_path = os.path.join(image_file_path_prefix, sys.argv[1])

image = itkio.read_image(image_path, image_type)

print itkio.get_dimensions(image)
print image.GetSpacing()

image_iocbio = ImageStack.import_from_itk(image, image_type)
print image_iocbio.get_shape()
print image_iocbio.get_voxel_sizes()
print image_iocbio.images.shape
print image_iocbio.images.ndim
