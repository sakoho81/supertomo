__author__ = 'sami'

import libtiff
import numpy
from matplotlib import pyplot as plt

tiff_file = libtiff.TiffFile('/tmp/tmpunOfmZ-supertomo.fuse/result_10.tif', mode='r')

tiff_data = tiff_file.get_tiff_array()

image = numpy.zeros(tiff_data.shape, dtype=tiff_data.dtype)

for i in range(len(tiff_data)):
    image[i] = tiff_data[i]

max = numpy.percentile(image, 99.99)

print max
hist, bins = numpy.histogram(image[image <= max], bins=60)
width = 0.7 * (bins[1] - bins[0])
center = (bins[:-1] + bins[1:]) / 2
plt.bar(center, hist, align='center', width=width)
plt.show()



