__author__ = 'sami'

import sys
import os
import datetime

from iocbio.microscope import itkutils
from iocbio.io import itkio

path_prefix = '/home/sami/Pictures/SuperTomo'

if len(sys.argv) < 4:
    print 'Error: You must specify an input image, image type and a transform'
    sys.exit(1)

# GET COMMAND LINE ARGS
image_path = sys.argv[1]
image_type = sys.argv[2]
transform_path = sys.argv[3]

#itk_utils.hide_warnings()

# LOAD IMAGE
image_path = os.path.join(path_prefix, image_path)
image = itkio.read_image(image_path, image_type)

# LOAD TRANSFORM
if not os.path.isfile(transform_path):
    transform_path = os.path.join(path_prefix, transform_path)
    if not os.path.isfile(transform_path):
        print 'Not a valid file %s' % transform_path
        sys.exit(1)

transform = itkio.read_transform(transform_path)

# Rotate PSF
image = itkutils.rotate_psf(
    image,
    transform,
    image_type,
    convert_to_itk=False,
    center_of_mass=True
)

# OUTPUT
output_dir = os.path.join(path_prefix, 'test_temp')
if not os.path.exists(output_dir):
        os.makedirs(output_dir)
file_name = datetime.datetime.now().strftime("%Y_%m_%d_%M") + \
                        '_psf_rotate' + \
                        '.mhd'
file_name = os.path.join(output_dir, file_name)

itkio.write_image(image, file_name, image_type)


