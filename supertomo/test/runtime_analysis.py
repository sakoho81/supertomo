import pstats
import sys

default_path = '/home/sami/timing_results.txt'

if len(sys.argv) < 2:
    path = default_path
else:
    path = sys.argv[1]

stats = pstats.Stats(path)

#Registration time
stats.print_stats('itk_registration')

stats.print_stats('gaussian')