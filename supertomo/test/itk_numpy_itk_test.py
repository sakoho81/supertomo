import os
import sys
from iocbio.io import itkio, image_stack

image_file_path_prefix = '/home/sami/Pictures/TestImages'
image_type='IUC3'

if len(sys.argv) < 2:
    print 'You should give an image file as a parameter'
    sys.exit(1)

image_path = os.path.join(image_file_path_prefix, sys.argv[1])

print "Reaging ITK image"
image = itkio.read_image(image_path, image_type)

iocbio_image = image_stack.ImageStack.import_from_itk(image, image_type)
print "Converting to IOCBIO stack"

print "Voxel sizes go now as follows:"
print len(iocbio_image.get_voxel_sizes())

image = iocbio_image.export_to_itk(image_type)


