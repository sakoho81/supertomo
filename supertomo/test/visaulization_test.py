import sys
import os

from numpy import random
from mayavi import mlab

from iocbio.io import itkio
from iocbio.microscope import itk_utils

image_file_path_prefix = '/home/sami/Pictures/TestImages'


if len(sys.argv) < 3:
    print "Error: You must specify a data file to visualize, and " \
          "a visualization method"
    sys.exit(1)

if len(sys.argv) == 4:
    image_type = sys.argv[3]
else:
    image_type = 'IUC3'

image_path = os.path.join(image_file_path_prefix, sys.argv[1])

image = itkio.read_image(image_path, image_type)

print image.__class__.__name__


array = itk_utils.convert_to_numpy(image, image_type)

print array.__class__.__name__

plot_method = sys.argv[2]
if 'surface' in plot_method:
    mlab.contour3d(array)
    mlab.show()
elif 'volume' in plot_method:
    mlab.pipeline.volume(mlab.pipeline.scalar_field(array))
    mlab.show()
else:
    print "Visualization mehtod %s not implemented" % plot_method
    sys.exit(1)