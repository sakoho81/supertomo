import sys
import os
import datetime

from iocbio.io import itkio
from iocbio.microscope import itkutils

image_file_path_prefix = '/home/sami/Pictures/CLEM'
image_type = 'IUC2'

if len(sys.argv) < 3:
    print 'You should give an image file and threshold value as parameters'
    sys.exit(1)

input_file = sys.argv[1]
threshold_value = int(sys.argv[2])

image_path = os.path.join(image_file_path_prefix, input_file)
assert os.path.isfile(image_path)

image = itkio.read_image(image_path, image_type)
filtered = itkutils.threshold_image_filter(image, threshold_value, image_type)

output_dir = os.path.join(image_file_path_prefix, 'test_temp')
suffix = os.path.splitext(input_file)[1]

if not os.path.exists(output_dir):
        os.makedirs(output_dir)

file_name = datetime.datetime.now().strftime("%Y_%m_%d_%M") + \
                        '_threshold' + \
                        suffix
file_name = os.path.join(output_dir, file_name)

itkio.write_image(filtered, file_name, image_type)