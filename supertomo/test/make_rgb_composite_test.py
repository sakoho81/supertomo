"""
Testing the create composite image functions in itkio module
"""

import sys
import os
import datetime

from iocbio.io import itkio

image_file_path_prefix = '/home/sami/Pictures/TestImages'
image_type = 'IUC2'

if len(sys.argv) < 3:
    print 'You should give filenames for two images to join'
    sys.exit(1)

red_path = os.path.join(image_file_path_prefix, sys.argv[1])
green_path = os.path.join(image_file_path_prefix, sys.argv[2])
assert os.path.isfile(red_path)
assert os.path.isfile(green_path)

red_image = itkio.read_image(red_path, image_type)
green_image = itkio.read_image(green_path, image_type)

file_name = datetime.datetime.now().strftime("%H-%M-%S") + \
                    '-composite' + \
                    '.tif'
output_dir = os.path.join(image_file_path_prefix, 'Results')
if not os.path.exists(output_dir):
        os.makedirs(output_dir)
output_path = os.path.join(output_dir, file_name)

itkio.write_composite_image((red_image, green_image), output_path, image_type)
