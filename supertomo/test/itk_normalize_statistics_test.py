__author__ = 'Sami Koho'

import sys
import os

from iocbio.microscope import itk_utils
from iocbio.io import itkio

image_file_path_prefix = '/home/sami/Pictures/TestImages'

if len(sys.argv) < 3:
    print 'Error: You must specify an input image'
    sys.exit(1)

image_type = sys.argv[2]

image_path = os.path.join(image_file_path_prefix, sys.argv[1])

image = itkio.read_image(image_path, image_type)

if '2' in image_type:
    image = itk_utils.type_cast(image, image_type, 'ID2')
    image_type = 'ID2'
else:
    image = itk_utils.type_cast(image, image_type, 'ID3')
    image_type = 'ID3'

mean, variance = itk_utils.get_image_statistics(image, image_type, verbal=False)

print image_type

print "Statistics for the original image."
print "Mean: %s" % mean
print "Variance: %s" % variance

image = itk_utils.normalize_image_filter(image, image_type)

mean, variance = itk_utils.get_image_statistics(image, image_type, verbal=False)

print "Statistics for the normalized image."
print "Mean: %s" % mean
print "Variance: %s" % variance

image = itk_utils.type_cast(image, image_type, sys.argv[2], verbal=False)
image = itk_utils.rescale_intensity(image, image_type, image_type)
image_type = sys.argv[2]

mean, variance = itk_utils.get_image_statistics(image, image_type, verbal=False)

print "Statistics for the rescaled image."
print "Mean: %s" % mean
print "Variance: %s" % variance

itkio.write_image(image, 'temp.mhd', image_type)