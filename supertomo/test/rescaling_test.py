__author__ = 'sami'


import sys
import os
import itk
from iocbio.io import itk_io_utilites as itkio
from iocbio.microscope import itk_utils as itkutils

image_file_path_prefix = '/media/sf_Sami/Microscope images/TestImages'

if len(sys.argv) < 2:
    print 'You should give an image file as a parameter'
    sys.exit(1)

image_path = os.path.join(image_file_path_prefix, sys.argv[1])

image = itkio.itk_read_image(image_path)
"""
print itkio.itk_get_dimensions(image)
print image.GetSpacing()

iso_image = itkutils.resample_to_isotropic(image)

print itkio.itk_get_dimensions(iso_image)
print iso_image.GetSpacing()
"""
image_path = os.path.join(image_file_path_prefix, 'test.tiff')
itkio.itk_write_image(image, image_path)