__author__ = 'Sami Koho'

import sys
import os
import datetime

from iocbio.io import itkio
from iocbio.microscope import itk_utils

image_file_path_prefix = '/home/sami/Pictures/TestImages'
image_type = 'IUC2'

if len(sys.argv) < 3:
    print 'You should give an image file and filter radius as parameters'
    sys.exit(1)

image_path = os.path.join(image_file_path_prefix, sys.argv[1])
image = itkio.read_image(image_path, image_type)

filtered = itk_utils.gaussian_blurring_filter(image, image_type, float(sys.argv[2]))

output_dir = os.path.join(image_file_path_prefix, 'Results')

suffix = os.path.splitext(sys.argv[1])[1]

if not os.path.exists(output_dir):
        os.makedirs(output_dir)

file_name = datetime.datetime.now().strftime("%Y_%m_%d_%M") + \
                        '_blur' + \
                        suffix
file_name = os.path.join(output_dir, file_name)

itkio.write_image(filtered, file_name, image_type)