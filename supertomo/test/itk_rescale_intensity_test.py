"""
A test for ITK intenisty rescaling/normalization function
"""
import sys
import os
import datetime

from iocbio.microscope import itk_utils
from iocbio.io import itk_io_utilites as itkio

image_file_path_prefix = '/media/sf_Sami/Microscope images/TestImages'

if len(sys.argv) < 3:
    print 'Error: You must specify an input image and a image type'
    sys.exit(1)

image_type = sys.argv[2]

image_path = os.path.join(image_file_path_prefix, sys.argv[1])

image = itkio.itk_read_image(image_path, image_type)
image = itk_utils.rescale_intensity(image, image_type, image_type)

output_dir = os.path.join(image_file_path_prefix, 'test_temp')

suffix = os.path.splitext(sys.argv[1])[1]

if not os.path.exists(output_dir):
        os.makedirs(output_dir)

file_name = datetime.datetime.now().strftime("%Y_%m_%d-%H_%M") + \
                        '_rescaling' + \
                        '_' + \
                        suffix
file_name = os.path.join(output_dir, file_name)

itkio.itk_write_image(image, file_name, image_type)


