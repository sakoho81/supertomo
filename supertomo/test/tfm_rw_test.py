__author__ = 'sami'
import sys
import itk
import iocbio.io.itk_io_utilites as itkio

path="transform.txt"

transform = itk.VersorRigid3DTransform.D.New()

if itkio.save_transform_to_file(path, transform):
    print "Transform succesfully saved to transform.txt"
else:
    print "Something is not quite right"
    sys.exit(-1)

transform2 = itkio.get_transform_from_file(path)

print transform
print transform2
