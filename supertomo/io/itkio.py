"""
itkio.py

Copyright (C) 2014 Sami Koho
All rights reserved.

This software may be modified and distributed under the terms
of the BSD license.  See the LICENSE file for details.

"""
import warnings
warnings.filterwarnings('ignore')
import itk
import os
from numpy import float64 as double
from numpy import float32 as single

data_file_types = ['.txt', '.dat', '.tfm']


def read_image(path, image_type='IUC3'):
    """
    To read an image from an image file to ITK

    The python wrapping in ITK requires the image_type to be provided
    as an argument to most function types. This is a bit clumsy, and
    therefore getattr() function together with an image_type string
    is used in here. All the image types recognized by ITK are accepted
    """
    itk_image_reader = getattr(itk.ImageFileReader, image_type).New()

    assert os.path.isfile(path)

    itk_image_reader.SetFileName(path)
    itk_image_reader.Update()

    return itk_image_reader.GetOutput()


def get_dimensions(image, flip_dims=False):
    """
    This function returns the dimensions of an image
    """
    region = image.GetLargestPossibleRegion()
    size = region.GetSize()
    size = tuple(size)
    if flip_dims:
        size = size[-1::]
    return size


def write_image(image, path, input_type, output_type=None):
    """
    Writes and image file by utilizing ITK IO functions. All
    of the file formats supported by ITK (and SCIFIO) are
    allowed.

    Parameters:

    image - ITK:Image object
    path - path to the file to save
    image_type - the pixel type of the ITK:Image
    output_type - same as above, if something else is not specified

    """
    if output_type is None:
        output_type = input_type

    rescaling = getattr(
        itk.RescaleIntensityImageFilter, input_type+output_type).New()
    rescaling.SetInput(image)

    writer = getattr(itk.ImageFileWriter, output_type).New()
    writer.SetInput(rescaling.GetOutput())
    writer.SetFileName(path)
    writer.Update()
    print "Image data saved to %s" % path


def write_transform(path, transform):
    """
    There is a ITKTransformFileWriter class in
    ITK for doing this same thing, but the wrapping was not
    working upon writing of this code. Therefore a simple
    Python version for saving the file was created
    """
    proper_name = False
    for suffix in data_file_types:
        if suffix in path:
            proper_name = True

    if proper_name:
        writer = itk.TransformFileWriterTemplate.New()
        writer.SetFileName(path)
        writer.SetInput(transform)
        writer.Update()

        return True

    else:
        print 'Not a file data file type. Please specify one of ' \
              'the following: %s' % ''.join(data_file_types)
        return False


def read_transform(path):
    """
    Read ITK transform from file. The syntax is expected to follow
    that used in itk::TransformFileWriterTemplate class.

    The transform file should include the following lines:
    1. Transform, which defines the transformation type and
       number format (float/double).
    2. Parameters, which defines the transform matrix, rotations
       and translations
    3. Fixed parameters, defines the origin (usually centre of mass
       in registration, but can be geometrical image centre as well)

    Parameters:

    path = path to a transform file

    Returns:

    ITK::Transform object initialized and ready to use,
    if reading succeeds; None, if any Error
    is encountered

    """
    # Check that the path is a file and that the file can be
    # a transform file
    if not os.path.isfile(path):
        print 'Not a valid filename: %s', path
        return None

    proper_name = False
    for suffix in data_file_types:
        if suffix in path:
            proper_name = True

    if proper_name:
        transform = None
        parameters = None
        fixed_parameters = None
        number_type = None

        with open(path, 'r') as f:
            for line in f:
                # Line that starts with Transform includes the transform
                # type definition. This should always come before any
                # other line, as the instantiated transform will be
                # utilised in the following cases.
                if line.startswith('Transform:'):
                    type_string = line.split(': ')[1].split('_')
                    transform_type = type_string[0]
                    if type_string[1] == 'double':
                        number_type = 'D'
                    elif type_string[1] == 'float':
                        number_type = 'F'
                    else:
                        return None

                    transform = getattr(
                        getattr(itk, transform_type), number_type
                    ).New()
                # Parameters contain the Transform matrix
                elif line.startswith('Parameters:'):
                    parameters = line.split(': ')[1].split(' ')
                    if number_type is 'D':
                        parameters = [
                            double(i)
                            for i in parameters if (i != '' and i != '\n')
                        ]
                    else:
                        parameters = [
                            single(i)
                            for i in parameters if (i != '' and i != '\n')
                        ]

                    if transform is None:
                        print "Corrupted file type. A transform type must " \
                              "be specified before parameters"
                        return None

                    itk_params = transform.GetParameters()

                    if len(itk_params) != len(parameters):
                        print "Number of parameters does not match"
                        return None

                    for i in range(len(parameters)):
                        itk_params[i] = parameters[i]

                    transform.SetParameters(itk_params)

                # Fixed parameters should be the last line in the transform
                # file. It contains information about the image origin.
                elif line.startswith('FixedParameters:'):
                    fixed_parameters = line.split(': ')[1].split(' ')
                    if number_type is 'D':
                        fixed_parameters = [
                            double(e)
                            for e in fixed_parameters if (e != '' and e != '\n')
                        ]
                    else:
                        fixed_parameters = [
                            single(e)
                            for e in fixed_parameters if (e != '' and e != '\n')
                        ]

                    itk_fixed = transform.GetFixedParameters()

                    if len(itk_fixed) != len(fixed_parameters):
                        print "Number of fixed parameters does not match"
                        return None

                    for i in range(len(fixed_parameters)):
                        itk_fixed[i] = fixed_parameters[i]

                    transform.SetFixedParameters(itk_fixed)

            if transform is None or parameters is None or fixed_parameters is None:
                print "Something is not quite right in the tranform file"
                return None
            else:
                return transform


def make_composite_rgb_image(images, image_type):
    """
    A utitity to combine several grayscale images into a single RGB image.

    :param images:      A tuple of itk:Image objects
    :param image_type:  Image type string, according to the notation in ITK.
    :return:            Returns a RGB composite image.
    """
    assert isinstance(images, tuple)
    assert len(images) <= 3

    composer = getattr(
        itk.ComposeImageFilter, image_type+'IRGB'+image_type[1:]).New()
    for i in range(len(images)):
        composer.SetInput(i, images[i])

    if len(images) < 3:
        zero_image = create_zero_image_copy(images[0], image_type)
        for i in range(3-len(images)):
            index = i+len(images)
            composer.SetInput(index, zero_image)

    composer.Update()

    return composer.GetOutput()


def write_composite_image(images, path, image_type):
    """
    An image file writer, to create a single RGB output from up-to
    three individual images.

    :param images:      A tuple of itk:Image objects
    :param path:        A full path to the image file ot be written
    :param image_type:  ITK image type. Only unsigned images are
                        currently supported in Python wrapping
    :return:
    """
    rgb_image = make_composite_rgb_image(images, image_type)
    if len(get_dimensions(images[0])) == 2:
        rgb_output_type = 'IRGBUC2'
    else:
        rgb_output_type = 'IRGBUC3'

    writer = getattr(itk.ImageFileWriter, rgb_output_type).New()
    writer.SetInput(rgb_image)
    writer.SetFileName(path)
    writer.Update()
    print "Image data saved to %s" % path


def create_zero_image_copy(image, image_type):
    """
    A function to create an empty copy of a reference image, filled
    with zeros. This can be used with the ComposeImageFilter, to fill
    blanks.

    :param image:       an ITK:Image Object
    :param image_type:  an image type string (e.g. IUC2)
    :return:            returns a new image of the same size and spacing as
                        the input image, filled with zeros
    """
    image_new = getattr(itk.Image, image_type[1:]).New()

    region = image.GetLargestPossibleRegion()
    image_new.SetRegions(region)
    image_new.Allocate()

    spacing = image.GetSpacing()
    image_new.SetSpacing(spacing)

    zero = getattr(itk.NumericTraits, image_type[1:-1]).ZeroValue()
    image_new.FillBuffer(zero)

    return image_new




