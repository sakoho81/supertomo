import numpy
from scipy import ndimage

__all__ = ['estimate_snr']

def estimate_snr(image,
                 noise_type = 'poisson',
                 use_peak = False):
    """
    Estimate signal-to-noise ratio of the image stack.

    Signal-to-noise ratio is defined as follows::

      SNR = E(image) / (sqrt(E(image - E(image))^2)

    where E(image) is the averaged image. In theory, calculating
    averaged image requires multiple acquisitions of the same image.
    E(image) can be estimated by smoothing the image with a gaussian.

    Parameters
    ----------
    image : `supertomo.io.image_stack.ImageStack`

    noise_type : {'poisson'}

    Notes
    -----

    In the case of Poisson noise the signal-to-noise ratio
    is defined as peak signal-to-noise ratio:

      SNR = sqrt(E(max image)).

    """

    if noise_type == 'poisson':
        if use_peak:
            peak_val = image.max()
        else:
            data_smooth = ndimage.uniform_filter(image, size=5, mode='nearest')
            peak_val = numpy.amax(data_smooth)

        return numpy.sqrt(peak_val)

    else:
        raise NotImplementedError(`noise_type`)