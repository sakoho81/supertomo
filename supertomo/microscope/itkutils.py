"""
itkutils.py

Copyright (C) 2014 Sami Koho
All rights reserved.

This software may be modified and distributed under the terms
of the BSD license.  See the LICENSE file for details.

This file contains several utilities & filters for simplified
usage of ITK (www.itk.org) modules in Python. Most of the ITK classes
have been implemented in similar manner, so it should be rather
easy to include additional filters.

"""
import warnings
warnings.filterwarnings('ignore')
import itk
import sys

from ..io import image_stack, itkio

def convert_to_numpy(itk_image, image_type):
    """
    A simple conversion function from ITK:Image to a Numpy array. Please notice
    that the pixel size information gets lost in the conversion. If you want
    to conserve image information, rather use ImageStack class method in
    supertomo.io.image_stack module
    """
    return getattr(itk.PyBuffer, image_type).GetArrayFromImage(itk_image)



def resample_image(image, transform, image_type, reference=None):
    """
    Resampling filter for manipulating data volumes. This function can be
    used to transform an image module or to perform up or down sampling
    for example.

    image       =   input image object itk::Image
    transform   =   desired transform itk::Transform
    image_type  =   pixel type of the image data
    reference   =   a reference image, which can be used in resizing
                    applications, when different dimensions and or
                    spacing are desired to the output image
    """
    if reference is None:
        reference = image

    resampler = getattr(itk.ResampleImageFilter, image_type+image_type).New()
    resampler.SetTransform(transform)


    resampler.SetInput(image)
    region = reference.GetLargestPossibleRegion()


    # interpolator = getattr(
    #    itk.NearestNeighborInterpolateImageFunction,
    #    image_type+'D').New()
    # resampler.SetInterpolator(interpolator)

    resampler.SetSize(region.GetSize())
    resampler.SetOutputOrigin(reference.GetOrigin())
    resampler.SetOutputSpacing(reference.GetSpacing())
    resampler.SetOutputDirection(reference.GetDirection())
    resampler.SetDefaultPixelValue(0)
    resampler.Update()

    return resampler.GetOutput()


def rotate_psf(psf, transform, image_type, convert_to_itk=False,
               return_image_stack=False, center_of_mass=False):
    """
    In case, only one point-spread-function (PSF) is to be used in the image
    fusion, it needs to be rotated with the transform of the moving_image.
    The transform is generated during the registration process.

    psf             = SuperTomo:ImageStack object, containing PSF data
    transform       = itk::Transform object
    image_type      = pixel type of the image
    convert_to_itk  = tells the function that the input image (psf) is a
                      ImageStack object, which must be converted to an
                      itk:Image object
    return_numpy    = it is possible to either return the result as an
                      itk:Image, or a ImageStack.

    """

    if convert_to_itk:
        image = psf.export_to_itk()
    else:
        image = psf

    transform_type = transform.GetNameOfClass()

    if 'VersorRigid3DTransform' in transform_type:
        # Set translation to zero
        itk_params = transform.GetParameters()

        for i in range(3, 6):
            itk_params[i] = 0.0

        transform.SetParameters(itk_params)

        # Find  and set center of rotation
        center = calculate_center_of_image(image, image_type,
                                           center_of_mass=center_of_mass)

        fixed_params = transform.GetFixedParameters()

        for i in range(len(fixed_params)):
            fixed_params[i] = center[i]

        transform.SetFixedParameters(fixed_params)
    else:
        print 'The PSF rotation function has currently been implemented for' \
              'VersorRigi3DTransform only. The provided transform type' \
              '%s could not be used' % transform_type
        return psf

    # Rotate
    image = resample_image(image, transform, image_type)

    if return_image_stack:
        return image_stack.ImageStack.import_from_itk(image, image_type)
    else:
        return image


def resample_to_isotropic(itk_image, image_type):
    """
    This function can be used to rescale or upsample a confocal stack,
    which generally has a larger spacing in the z direction.

    :param itk_image:   an ITK:Image object
    :param image_type:  an ITK image type string (e.g. 'IUC3')
    :return:            returns a new ITK:Image object with rescaled
                        axial dimension
    """
    filter = getattr(itk.ResampleImageFilter, image_type+image_type).New()
    transform = itk.IdentityTransform.D3.New()
    interpolator = itk.LinearInterpolateImageFunction.IUC3D.New()

    filter.SetDefaultPixelValue(0)
    filter.SetInterpolator(interpolator)
    # Set output spacing
    spacing = itk_image.GetSpacing()

    if len(spacing) != 3:
        print "The function resample_to_isotropic(itk_image, image_type) is" \
              "intended for processing 3D images. The input image has %d " \
              "dimensions" % len(spacing)

    scaling = spacing[2]/spacing[0]

    for i in range(3):
        spacing[i] = spacing[0]
        
    filter.SetOutputSpacing(spacing)
    filter.SetOutputDirection(itk_image.GetDirection())
    filter.SetOutputOrigin(itk_image.GetOrigin())

    # Set Output Image Size
    region = itk_image.GetLargestPossibleRegion()
    size = region.GetSize()
    size[2] = int(size[2]*scaling)
    filter.SetSize(size)

    transform.SetIdentity()
    filter.SetTransform(transform)
    filter.SetInput(itk_image)

    filter.Update()

    return filter.GetOutput()


def rescale_intensity(image, input_type, output_type='IUC3'):
    """
    A filter to scale the intensities of the input image to the full range
    allowed by the pixel type

    Inputs:
        image       = an itk.Image() object
        input_type  = pixel type string of the input image. Must be an ITK
                      recognized pixel type
        output_type = same as above, for the output image
    """
    rescaling = getattr(
        itk.RescaleIntensityImageFilter, input_type+output_type).New()
    rescaling.SetInput(image)
    rescaling.Update()

    return rescaling.GetOutput()


def gaussian_blurring_filter(image, image_type, variance):
    """
    Gaussian blur filter
    """

    filter = getattr(
        itk.DiscreteGaussianImageFilter, image_type+image_type).New()
    filter.SetInput(image)
    filter.SetVariance(variance)
    filter.Update()

    return filter.GetOutput()


def grayscale_dilate_filter(image, image_type, kernel_radius):
    """
    Grayscale dilation filter for 2D/3D datasets
    """

    if '2' in image_type:
        image_type = image_type + image_type + 'SE2'
    else:
        image_type = image_type + image_type + 'SE3'

    filter = getattr(itk.GrayscaleDilateImageFilter, image_type).New()

    kernel = filter.GetKernel()
    kernel.SetRadius(kernel_radius)
    kernel = kernel.Ball(kernel.GetRadius())

    filter.SetKernel(kernel)
    filter.SetInput(image)

    filter.Update()

    return filter.GetOutput()

def mean_filter(image, image_type, kernel_radius):
    """
    Uniform Mean filter for itk.Image objects
    """
    filter = getattr(itk.MeanImageFilter, image_type+image_type).New()

    filter.SetRadius(kernel_radius)
    filter.SetInput(image)

    filter.Update()

    return filter.GetOutput()

def median_filter(image, image_type, kernel_radius):
    """
    Median filter for itk.Image objects

    :param image:           an itk.Image object
    :param image_type:      image type string (e.g. IUC2, IF3)
    :param kernel_radius:   median kernel radius
    :return:                filtered image
    """
    filter = getattr(itk.MedianImageFilter, image_type+image_type).New()
    #radius = filter.GetRadius()
    #radius.Fill(kernel_radius)
    filter.SetRadius(kernel_radius)
    filter.SetInput(image)

    filter.Update()

    return filter.GetOutput()



def normalize_image_filter(image, image_type):
    """
    Normalizes the pixel values in an image to Mean of zero and Variance
    of one. A floating point image_type is expected. For integer pixel
    type, casting to a float is recommended before using this.
    """

    filter = getattr(itk.NormalizeImageFilter, image_type+image_type).New()
    filter.SetInput(image)
    filter.Update()

    return filter.GetOutput()

def threshold_image_filter(image, threshold, image_type, th_value=0,
                           th_method="below"):
    """
    Thresholds an image by setting pixel values above or below "threshold"
    to "th_value". The result is not a binary image, but a thresholded
    grayscale image.
    """

    filter = getattr(itk.ThresholdImageFilter, image_type).New()
    filter.SetInput(image)

    if th_method is "above":
        filter.ThresholdAbove(threshold)
    elif th_method is "below":
        filter.ThresholdBelow(threshold)

    filter.SetOutsideValue(th_value)

    filter.Update()

    return filter.GetOutput()


def get_image_statistics(image, image_type, verbal=True):
    """
    A utility to calculate basic image statistics (Mean and Variance here)

    :param image:       an ITK:Image object
    :param image_type:  a string describing the image type (e.g. IUC3). The
                        naming convention as in ITK
    :param verbal:      print results on screen (ON/OFF)
    :return:            returns the image mean and variance in a tuple
    """
    filter = getattr(itk.StatisticsImageFilter, image_type).New()
    filter.SetInput(image)
    filter.Update()

    mean = filter.GetMean()
    variance = filter.GetVariance()

    if verbal is True:
        print "Mean: %s" % mean
        print "Variance: %s" % variance



    return mean, variance


def type_cast(image, input_type, output_type):
    """
    A utility for changing the image pixel container type

    :param image:       An ITK:Image
    :param input_type:  input image type as a string (e.g. IUC3)
    :param output_type: output image type as a string (e.g. IF2)
    :return:            returns the image with new pixel type
    """
    filter = getattr(itk.CastImageFilter, input_type+output_type).New()
    filter.SetInput(image)
    filter.Update()

    return filter.GetOutput()


def calculate_center_of_image(image, image_type, center_of_mass=False):
    """
    Center of an image can be defined either geometrically or statistically,
    as a Center-of-Gravity measure.

    Based on itk::ImageMomentsCalculator
    http://www.itk.org/Doxygen/html/classitk_1_1ImageMomentsCalculator.html
    """
    if center_of_mass:
        calculator = getattr(itk.ImageMomentsCalculator, image_type).New()
        calculator.SetImage(image)
        calculator.Compute()

        center = tuple(calculator.GetCenterOfGravity())
    else:
        imdims = itkio.get_dimensions(image)
        imspacing = tuple(image.GetSpacing())

        center = map(
            lambda size, spacing: spacing * size / 2,
            imdims, imspacing
        )

    return center


def get_image_subset(image, image_type, multiplier):
    """

    A simple utility for extracting a subset of an ItkImage, based
    on a single floating point multiplier.

    Based on itk::ExtractImageFilter
    http://www.itk.org/Doxygen/html/classitk_1_1ExtractImageFilter.html

    Two parameters: output_size and output_origin define the subset.
    Here the parameters are calculated with a single multiplier.

    """

    filter = getattr(itk.ExtractImageFilter, image_type+image_type).New()

    input_region = image.GetLargestPossibleRegion()
    input_origin = input_region.GetIndex()
    input_size = input_region.GetSize()

    output_region = input_region
    output_size = input_size
    output_origin = input_origin

    for i in range(len(output_size)):
        output_size[i] = int(multiplier*input_size[i])
        output_origin[i] = int(0.5*(input_size[i]-output_size[i]))

    output_region.SetSize(output_size)
    output_region.SetIndex(output_origin)

    filter.SetInput(image)
    filter.SetExtractionRegion(output_region)
    filter.Update()

    return filter.GetOutput()

























