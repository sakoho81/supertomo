"""
plots.py

Copyright (C) 2014 Sami Koho
All rights reserved.

This software may be modified and distributed under the terms
of the BSD license.  See the LICENSE file for details.

Functions in this file can be used to genearte different kinds of plots.

Mayavi2 is used to produce 3D plots of image volumes. Various type of inputs
can be accepted

matplotlib is used to generate 2D plots of registration, fusion/deconvolution
results. The data is saved into a RowFile object (supertomo.io.io)
"""

import numpy

from mayavi import mlab
from matplotlib import pyplot as plt
from matplotlib import cm

from supertomo.io import io, itkio
from supertomo.microscope import itkutils


def plot_3d_volume(image, image_type='IUC3', plot_method='surface', load=False):
    """
    A function for producing a 3D volume rendering from single 3D image
    with Mayavi.

    :param image:       Multiple image types are accepted. At the moment the
                        input image can be an ITK:Image object (registration),
                        an supertomo ImageStack object or a Numpy array.
                        Alternatively an image can  be a path, in which case
                        the data will be loaded from a file. In order to use
                        this functionality, the load parameter should be set
                        to True
    :param image_type:  An image type string. The naming convention used in
                        ITK is followed here. Can normally be set through
                        the command line interface
    :param plot_method: Defines which kind of a plot method should be used.
                        Surface and volume rendering is currently supported
    :param load:        Set to True if you are passing a path to an image
                        instead of an actual image
    :return:            Boolean: False, if there is a problem with the
                        supplied parameters
    """

    if load is True:
        image = itkio.read_image(image, image_type)

    class_str = image.__class__.__name__

    if 'itkImage' in class_str:
        array = itkutils.convert_to_numpy(image, image_type)
    elif 'ndarray' in class_str:
        array = image
    elif 'ImageStack' in class_str:
        array = image[:]
    else:
        print "Unknown data object %s" % class_str
        return False

    if 'surface' in plot_method:
        mlab.contour3d(array)
        mlab.show()
    elif 'volume' in plot_method:
        mlab.pipeline.volume(mlab.pipeline.scalar_field(array))
        mlab.show()
    else:
        print "Visualization mehtod %s not implemented" % plot_method
        return False


def plot_rowfile(path, shape=330, tight=True):
    """
    A quick and dirty function for plotting all the variables
    in a data file

    Input:

    path -  a complete path to a data file, which needs to be
            in RowFile format (see supertomo.io -> Rowfile

    shape - layout of the produced plot. Default 330 means,
            3 rows and 3 columns. The shape  (3*3) needs to be
            larger than the number of variables in the data file


    """
    data, titles = io.RowFile(path).read(with_titles=True)

    lst = map(int, str(shape))
    assert len(titles) <= lst[0]*lst[1]

    colors = cm.rainbow(numpy.linspace(0, 1, len(titles)))

    plot = 0
    for i, j in zip(titles, colors):
        temp = data[i]
        plt.subplot(shape+plot)
        plt.plot(
            numpy.arange(len(temp)),
            numpy.array(temp),
            linewidth=1.5,
            color=j
        )
        plt.title(i)
        plot += 1

    if tight:
        plt.tight_layout()

    plt.show()


def plot_single_from_rowfile(path, varname, plot_title="", y_label="",
                             x_label=""):

    """
    A function for generating a single plot of a specified variable in
    a data file. The data file must be in RowFile format (see supertomo.io)

    """
    data, titles = io.RowFile(path).read(with_titles=True)

    assert varname in titles

    yvals = data[varname]
    xvals = numpy.arange(len(yvals))

    plt.plot(
        xvals,
        yvals,
        linewidth=1.5,
        color='c',
        label=varname
    )
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(plot_title)

    plt.legend(loc=1)

    plt.show()




