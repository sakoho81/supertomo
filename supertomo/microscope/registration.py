"""
registration.py

Copyright (C) 2014 Sami Koho
All rights reserved.

This software may be modified and distributed under the terms
of the BSD license.  See the LICENSE file for details.

This file contains functions for registration of microscope image
volumes. The methods are based on Insight Toolkit (www.itk.org),
the installation of which is required in order to run the contained
functions

Currently (04/2014) rigid body registration of three-dimensional volumes
has been implemented. Several metrics 1. least-squares 2. viola-wells mutual
information 3. mattes mutual information are supported implemented

"""
import sys

import warnings
warnings.filterwarnings('ignore')
import itk
import tempfile
import os
from numpy import float64 as double
from numpy import pi

from ..io import io
from ..microscope import itkutils


def get_registration_options_group(parser):
    """
    Defines command line options for image registration functions. The
    methods are based on Insight Toolkit (www.itk.org) which is
    required to run the functions.

    Matte's Mutual Information  based registration introduces a number
    of new command line options for fine tuning its behaviour. All of the
    options have default values, which should work at least with the sample
    image sets.

    mattes_histogram_bins and mattes_spatial_samples define how samples are
    drawn from the moving and fixed images for the mutual information metric.

    set_rot_axis and set_rotation control the initial rotation
    of the moving image by the initializer. The axis of the major rotation
    (in case of 3D registration) should be selected and an estimate,
    in radians, for the rotation offset
    should be given. This will be optimized during registration, but sometimes
    giving a rough initial estimate helps in finding a registration match.

    set_translation_scale controls how translations and rotations relate
    to one another during registration. By default translation scale is
    1000 times that of rotation, as rotations of 1 radian are rare, but translations
    can be in range of tens or hundreds of pixels.

    min_step, max_step and reg_max_iterations control the registration
    speed and accuracy
    """
    parser.add_option(
        '--reg-method',
        dest='registration_method',
        choices=['mattes', 'mean-squared-difference', 'viola-wells',
                 'normalized-correlation'],
        default='mattes',
        help='Specify registration method'
    )
    parser.add_option(
        '--two-step',
        dest='two_step_registration',
        action='store_true',
        help='Select if you want to do a two phase registration, '
             'the first being with a degraded image and the second'
             'with the high-resolution original'
    )
    parser.add_option(
        '--normalize',
        action='store_true',
        help='Choose this option if you want to normalize the intensity values'
             'before registration. Some registration methods work better with'
             'normalized intensities.'
    )
    # It is possible to degrade the input images before registration
    # This sometimes helps with the registrations, as differences
    # at pixel level may get the registration stuck at the wrong
    # position
    parser.add_option(
        '--gaussian',
        dest='gaussian_variance',
        type='float',
        default=0.0,
        help='Define variance for Gaussian blur'
    )
    parser.add_option(
        '--dilation',
        dest='dilation_size',
        type='int',
        default=0,
        help='Define size for Grayscale dilation'
    )
    parser.add_option(
        '--mean',
        dest='mean_kernel',
        type='int',
        default=0,
        help='In case you would like to use a mean filter to smoothen the images'
             'before registration, define a kernel here'
    )
    parser.add_option(
        '--median',
        dest='median_size',
        type='int',
        default=0,
        help='Enable median filtering before registering by a non-zero kernel size'
    )

    # Mattes mutual information metric specific options
    parser.add_option(
        '--mattes-histogram-bins',
        dest='mattes_histogram_bins',
        type='int',
        default=15,
        help='Specify the number of histogram bins for Mattes '
             'Mutual Information sampling'
    )
    parser.add_option(
        '--mattes-spatial-samples',
        dest='mattes_spatial_samples',
        type='int',
        default=100000,
        help='Specify the number of samples to take from each '
             'histogram bin'
    )

    # Viola Wells mutual information specific parameters
    parser.add_option(
        '--vw-fixed-sd',
        dest='vw_fixed_sd',
        type='float',
        default=0.4,
        help='Specify the fixed image SD value in Viola-Wells mutual '
             'information registration'
    )
    parser.add_option(
        '--vw-moving-sd',
        dest='vw_moving_sd',
        type='float',
        default=0.4,
        help='Specify the fixed image SD value in Viola-Wells mutual '
             'information registration'
    )
    parser.add_option(
        '--vw-samples-multiplier',
        dest='vw_samples_multiplier',
        type='float',
        default=0.2,
        help='Specify the amount of spatial samples to be used in '
             'mutual information calculations. The amount is given'
             'as a proportion of the total number of pixels in the'
             'fixed image.'
    )

    # Initializer options
    parser.add_option(
        '--set-rot-axis',
        dest='set_rot_axis',
        type='int',
        default=0,
        help='Specify the axis for initial rotation of the '
             'moving image'
    )
    parser.add_option(
        '--set-rotation',
        dest='set_rotation',
        type='float',
        default=1.0,
        help='Specify an estimate for initial rotation angle'
    )
    parser.add_option(
        '--set-scale',
        dest='set_scale',
        type='float',
        default=1.0,
        help='Specify the initial scale for similarity transform'
    )
    # Optimizer options
    parser.add_option(
        '--set-translation-scale',
        dest='translation_scale',
        type='float',
        default=1.0,
        help='A scaling parameter to adjust optimizer behavior'
             'effect on rotation and translation. By default'
             'the translation scale is 1000 times that of rotation'
    )
    parser.add_option(
        '--set-scaling-scale',
        dest='scaling_scale',
        type='float',
        default=10.0
    )
    parser.add_option(
        '--max-step',
        dest='max_step_length',
        type='float',
        default=0.2,
        help='Specify an estimate for initial rotation angle'
    )
    parser.add_option(
        '--min-step',
        dest='min_step_length',
        type='float',
        default=0.000001,
        help='Specify an estimate for initial rotation angle'
    )
    parser.add_option(
        '--reg-max-iterations',
        dest='registration_max_iterations',
        type='int',
        default=200,
        help='Specify an estimate for initial rotation angle'
    )
    parser.add_option(
        '--reg-relax-factor',
        dest='relaxation_factor',
        type='float',
        default=0.5,
        help='Defines how quickly optmizer shortens the step size'
    )
    parser.add_option(
        '--reg-print-prog',
        dest='print_registration_progress',
        action='store_true'
    )
    parser.add_option(
        '--use-internal-type',
        dest='use_internal_type',
        action='store_true'
    )
    parser.add_option(
        '--disable-init-moments',
        dest='moments',
        action='store_false'
    )
    parser.add_option(
        '--threshold',
        type='int',
        default=0,
        help='Inserting an integer value larger than zero enables a grayscale'
             'threshold filter'
    )

def itk_registration_rigid_3d(fixed_image, moving_image, options,
                              initial_transform=None):
    """
    A Python implementation for a Rigid Body 3D registration, utilizing
    ITK (www.itk.org) library functions.

    :param fixed_image:
                            The reference image. Must be an instance of
                            itk.Image class. Numpy arrays can be converted
                            to this format with ITK PyBuffer
    :param moving_image:
                            The image for which the spatial transform will
                            be calculated. Same requirements as above.
    :param options:
                            Options provided by the user via CLI or the
                            included GUI. See script_options.py.
    :param initial_transform:
                            Initial transform may be provided by the user
                            instead of calculating it with an initializer.
                            The transform must be of the type
                            itk.VersorRigid3DTransform
    :return:
                            The final transform, an instance of
                            itk.VersorRigid3DTransform
    """

    print 'Setting up registration job'

    # Setup tempfile for saving registration progress
    data_file_path = tempfile.mkdtemp('-supertomo.register')
    data_to_save = ('Metric', 'X-versor', 'Y-versor', 'Z-versor', 'X-translation',
                    'Y-translation', 'Z-translation')
    data_file_name = os.path.join(data_file_path, 'registration_data.txt')
    data_file = io.RowFile(data_file_name, data_to_save, append=False)
    data_file.comment('Registration Command: %s' % (' '.join(map(str, sys.argv))))

    # Check if type conversions are needed.
    if options.use_internal_type is True:
        image_type = options.internal_type+'3'
    elif 'viola-wells' in options.registration_method:

        print "Normalizing images for Viola-Wells"
        # Cast to floating point. Can be single or double precision, depending
        # on the setting
        image_type = options.internal_type+'3'
        fixed_image = itkutils.type_cast(
            fixed_image,
            options.image_type,
            image_type
        )
        moving_image = itkutils.type_cast(
            moving_image,
            options.image_type,
            image_type
        )
        # Normalize images
        fixed_image = itkutils.normalize_image_filter(
            fixed_image,
            image_type
        )
        moving_image = itkutils.normalize_image_filter(
            moving_image,
            image_type
        )
    else:
        image_type = options.image_type

    fixed_image_region = fixed_image.GetBufferedRegion()

    # REGISTRATION COMPONENTS SETUP
    # ========================================================================
    transform = itk.VersorRigid3DTransform.D.New()
    optimizer = itk.VersorRigid3DTransformOptimizer.New()
    interpolator = getattr(itk.LinearInterpolateImageFunction,
                           image_type+'D').New()

    registration = getattr(itk.ImageRegistrationMethod,
                           image_type+image_type).New()

    if 'mattes' in options.registration_method:
        image_metric = getattr(itk.MattesMutualInformationImageToImageMetric,
                               image_type+image_type).New()
        image_metric.SetNumberOfHistogramBins(options.mattes_histogram_bins)
        image_metric.SetNumberOfSpatialSamples(options.mattes_spatial_samples)
    elif 'viola-wells' in options.registration_method:
        image_metric = getattr(itk.MutualInformationImageToImageMetric,
                               image_type+image_type).New()

        image_metric.SetFixedImageStandardDeviation(options.vw_fixed_sd)
        image_metric.SetMovingImageStandardDeviation(options.vw_moving_sd)

        # Calculate number of spatial samples as a fraction of total pixel count
        # of the fixed image
        pixels = fixed_image_region.GetNumberOfPixels()
        number_of_samples = int(options.vw_samples_multiplier*pixels)
        image_metric.SetNumberOfSpatialSamples(number_of_samples)

        optimizer.MaximizeOn()

    elif 'mean-squared-difference' in options.registration_method:
        image_metric = getattr(itk.MeanSquaresImageToImageMetric,
                               image_type+image_type).New()
    elif 'normalized-correlation' in options.registration_method:
        image_metric = getattr(itk.NormalizedCorrelationImageToImageMetric,
                               image_type+image_type).New()
        image_metric.SetFixedImageRegion(fixed_image.GetBufferedRegion())
    else:
        print "Registration method %s not implemented" \
              % options.registration_method
        return None

    # REGISTRATION
    # ========================================================================
    registration.SetMetric(image_metric)
    registration.SetOptimizer(optimizer)
    registration.SetTransform(transform)
    registration.SetInterpolator(interpolator)

    registration.SetFixedImage(fixed_image)
    registration.SetMovingImage(moving_image)
    registration.SetFixedImageRegion(fixed_image.GetBufferedRegion())

    if initial_transform is None:
        print 'Calculating initial registration parameters'
        # INITIALIZER
        # ====================================================================

        initializer = getattr(
            itk.CenteredVersorTransformInitializer,
            image_type+image_type).New()

        initializer.SetTransform(transform)
        initializer.SetFixedImage(fixed_image)
        initializer.SetMovingImage(moving_image)

        if options.moments:
            initializer.MomentsOn()

        initializer.InitializeTransform()

        # INITIAL PARAMETERS
        # ====================================================================

        # Initialize rotation
        rotation = transform.GetVersor()
        axis = rotation.GetAxis()

        for i in range(len(axis)):
            if i == options.set_rot_axis:
                axis[i] = 1.0
            else:
                axis[i] = 0.0

        rotation.Set(axis, double(options.set_rotation))

        transform.SetRotation(rotation)

    else:
        transform = initial_transform

    # Set initial parameters. The initializer will communicate directly with
    # transform
    registration.SetInitialTransformParameters(transform.GetParameters())

    print 'Setting up optimizer'

    # OPTIMIZER
    # ========================================================================

    # optimizer scale
    translation_scale = 1.0 / options.translation_scale

    optimizer_scales = itk.Array.D(transform.GetNumberOfParameters())
    optimizer_scales.SetElement(0, 1.0)
    optimizer_scales.SetElement(1, 1.0)
    optimizer_scales.SetElement(2, 1.0)
    optimizer_scales.SetElement(3, translation_scale)
    optimizer_scales.SetElement(4, translation_scale)
    optimizer_scales.SetElement(5, translation_scale)

    optimizer.SetScales(double(optimizer_scales))

    optimizer.SetMaximumStepLength(double(options.max_step_length))
    optimizer.SetMinimumStepLength(double(options.min_step_length))
    optimizer.SetNumberOfIterations(options.registration_max_iterations)

    optimizer.SetRelaxationFactor(double(options.relaxation_factor))


    # OBSERVER
    # ========================================================================
    def iteration_update():

        current_parameter = transform.GetParameters()
        optimizer_value = optimizer.GetValue()
        if options.print_registration_progress:
            print "M: %f   P: %f %f %f %f %f %f" % (
                optimizer_value,
                current_parameter.GetElement(0),
                current_parameter.GetElement(1),
                current_parameter.GetElement(2),
                current_parameter.GetElement(3),
                current_parameter.GetElement(4),
                current_parameter.GetElement(5)
            )
        tmplist = list()
        tmplist.append(optimizer_value)
        for j in range(len(current_parameter)):
            tmplist.append(current_parameter.GetElement(j))
        data_file.write(tmplist)

    iteration_command = itk.PyCommand.New()
    iteration_command.SetCommandCallable(iteration_update)
    optimizer.AddObserver(itk.IterationEvent(), iteration_command)

    # START
    # ========================================================================

    print "Starting registration"
    registration.Update()

    # Get the final parameters of the transformation
    #
    final_parameters = registration.GetLastTransformParameters()

    versor_x = final_parameters[0]
    versor_y = final_parameters[1]
    versor_z = final_parameters[2]
    final_translation_x = final_parameters[3]
    final_translation_y = final_parameters[4]
    final_translation_z = final_parameters[5]

    number_of_iterations = optimizer.GetCurrentIteration()
    best_value = optimizer.GetValue()

    print "Final Registration Parameters "
    print "Versor X  = %f" % versor_x
    print "Versor Y = %f" % versor_y
    print "Versor Z = %f" % versor_z
    print "Translation X = %f" % final_translation_x
    print "Translation Y = %f" % final_translation_y
    print "Translation Z = %f" % final_translation_z
    print "Iterations = %f" % number_of_iterations
    print "Metric value = %f" % best_value

    transform.SetParameters(final_parameters)
    matrix = transform.GetMatrix()
    offset = transform.GetOffset()

    print "Matrix = "
    for i in range(4):
        print "%f %f %f %f" % (
            matrix(i, 0),
            matrix(i, 1),
            matrix(i, 2),
            matrix(i, 3)
        )

    print "Offset = "
    print offset

    # GET TRANSFORM
    # ========================================================================

    final_transform = itk.VersorRigid3DTransform.D.New()
    final_transform.SetCenter(transform.GetCenter())
    final_transform.SetParameters(final_parameters)
    final_transform.SetFixedParameters(transform.GetFixedParameters())

    data_file.close()


    return final_transform


def itk_registration_rigid_2d(fixed_image, moving_image, options,
                              initial_transform=None):
    """
    A Python implementation for a Rigid Body 2D registration, utilizing
    ITK (www.itk.org) library functions.

    :param fixed_image:
                            The reference image. Must be an instance of
                            itk.Image class. Numpy arrays can be converted
                            to this format with ITK PyBuffer
    :param moving_image:
                            The image for which the spatial transform will
                            be calculated. Same requirements as above.
    :param options:
                            Options provided by the user via CLI or the
                            included GUI. See script_options.py.
    :param initial_transform:
                            Initial transform may be provided by the user
                            instead of calculating it with an initializer.
                            The transform must be of the type
                            itk.CenteredRigid2DTransform
    :return:
                            The final transform, an instance of
                            itk.CenteredRigid2DTransform
    """
    print 'Setting up registration job'

    # Setup tempfile for saving registration progress
    data_file_path = tempfile.mkdtemp('-supertomo.register')
    data_to_save = ('Metric', 'Rotation', 'CenterX', 'CenterY', 'X-translation',
                    'Y-translation')
    data_file_name = os.path.join(data_file_path, 'registration_data.txt')
    data_file = io.RowFile(data_file_name, data_to_save, append=False)
    data_file.comment('Registration Command: %s' % (' '.join(map(str, sys.argv))))

    # Check if type conversions are needed.
    if options.use_internal_type is True:
        image_type = options.internal_type+'2'
    elif 'viola-wells' in options.registration_method:

        print "Normalizing images for Viola-Wells"
        # Cast to floating point. Can be single or double precision, depending
        # on the setting
        image_type = options.internal_type+'2'
        fixed_image = itkutils.type_cast(
            fixed_image,
            options.image_type,
            image_type
        )
        moving_image = itkutils.type_cast(
            moving_image,
            options.image_type,
            image_type
        )
        # Normalize images
        fixed_image = itkutils.normalize_image_filter(
            fixed_image,
            image_type
        )
        moving_image = itkutils.normalize_image_filter(
            moving_image,
            image_type
        )
    else:
        image_type = options.image_type

    fixed_image_region = fixed_image.GetBufferedRegion()

    # REGISTRATION COMPONENTS SETUP
    # ========================================================================
    transform = itk.CenteredRigid2DTransform.New()
    optimizer = itk.RegularStepGradientDescentOptimizer.New()
    interpolator = getattr(itk.LinearInterpolateImageFunction,
                           image_type+'D').New()

    registration = getattr(itk.ImageRegistrationMethod,
                           image_type+image_type).New()

    if 'mattes' in options.registration_method:
        image_metric = getattr(itk.MattesMutualInformationImageToImageMetric,
                               image_type+image_type).New()
        image_metric.SetNumberOfHistogramBins(options.mattes_histogram_bins)
        image_metric.SetNumberOfSpatialSamples(options.mattes_spatial_samples)
    elif 'viola-wells' in options.registration_method:
        image_metric = getattr(itk.MutualInformationImageToImageMetric,
                               image_type+image_type).New()

        image_metric.SetFixedImageStandardDeviation(options.vw_fixed_sd)
        image_metric.SetMovingImageStandardDeviation(options.vw_moving_sd)

        # Calculate number of spatial samples as a fraction of total pixel count
        # of the fixed image
        pixels = fixed_image_region.GetNumberOfPixels()
        number_of_samples = int(options.vw_samples_multiplier*pixels)
        print number_of_samples
        image_metric.SetNumberOfSpatialSamples(number_of_samples)

        optimizer.MaximizeOn()

    elif 'normalized-correlation' in options.registration_method:
        image_metric = getattr(itk.NormalizedCorrelationImageToImageMetric,
                               image_type+image_type).New()
        image_metric.SetFixedImageRegion(fixed_image.GetBufferedRegion())

    elif 'mean-squared-difference' in options.registration_method:
        image_metric = getattr(itk.MeanSquaresImageToImageMetric,
                               image_type+image_type).New()
    else:
        print "Registration method %s not implemented" \
              % options.registration_method
        return None

    # REGISTRATION
    # ========================================================================
    registration.SetMetric(image_metric)
    registration.SetOptimizer(optimizer)
    registration.SetTransform(transform)
    registration.SetInterpolator(interpolator)

    registration.SetFixedImage(fixed_image)
    registration.SetMovingImage(moving_image)
    registration.SetFixedImageRegion(fixed_image.GetBufferedRegion())

    if initial_transform is None:
        print 'Calculating initial registration parameters'
        # INITIALIZER
        # ====================================================================
        init_type = 'CR2DTD'+image_type+image_type
        initializer = getattr(itk.CenteredTransformInitializer, init_type).New()

        initializer.SetTransform(transform)
        initializer.SetFixedImage(fixed_image)
        initializer.SetMovingImage(moving_image)

        initializer.MomentsOn()

        initializer.InitializeTransform()

        transform.SetAngle(double(options.set_rotation))

        # INITIAL PARAMETERS
        # ====================================================================

        registration.SetInitialTransformParameters(transform.GetParameters())

    else:
        assert isinstance(initial_transform, itk.CenteredRigid2DTransform)
        registration.SetInitialTransformParameters(initial_transform.GetParameters())

    # OPTIMIZER
    # ========================================================================
    print 'Setting up optimizer'
    # optimizer scale
    translation_scale = 1.0/options.translation_scale

    optimizer_scales = itk.Array.D(transform.GetNumberOfParameters())
    optimizer_scales.SetElement(0, 1.0)
    optimizer_scales.SetElement(1, translation_scale)
    optimizer_scales.SetElement(2, translation_scale)
    optimizer_scales.SetElement(3, translation_scale)
    optimizer_scales.SetElement(4, translation_scale)


    optimizer.SetScales(double(optimizer_scales))

    optimizer.SetMaximumStepLength(double(options.max_step_length))
    optimizer.SetMinimumStepLength(double(options.min_step_length))
    optimizer.SetNumberOfIterations(options.registration_max_iterations)

    optimizer.SetRelaxationFactor(double(options.relaxation_factor))


    # OBSERVER
    # ========================================================================

    def iteration_update():

        current_parameter = transform.GetParameters()
        optimizer_value = optimizer.GetValue()
        if options.print_registration_progress:
            print "M: %f   P: %f %f %f %f %f" % (
                optimizer_value,
                current_parameter.GetElement(0),
                current_parameter.GetElement(1),
                current_parameter.GetElement(2),
                current_parameter.GetElement(3),
                current_parameter.GetElement(4)
            )
        tmplist = list()
        tmplist.append(optimizer_value)
        for j in range(len(current_parameter)):
            tmplist.append(current_parameter.GetElement(j))
        data_file.write(tmplist)

    iteration_command = itk.PyCommand.New()
    iteration_command.SetCommandCallable(iteration_update)
    optimizer.AddObserver(itk.IterationEvent(), iteration_command)

    # START
    # ========================================================================

    print "Starting registration"
    registration.Update()

    # Get the final parameters of the transformation
    #
    final_parameters = registration.GetLastTransformParameters()

    final_angle = final_parameters[0]
    final_angle_in_degrees = final_angle*180.0/pi
    final_rotation_center_x = final_parameters[1]
    final_rotation_center_y = final_parameters[2]
    final_translation_x = final_parameters[3]
    final_translation_y = final_parameters[4]

    number_of_iterations = optimizer.GetCurrentIteration()
    best_value = optimizer.GetValue()

    print "Final Registration Parameters "
    print "Angle (degrees)  = %f" % final_angle_in_degrees
    print "Center of rotation:"
    print "X: %f" % final_rotation_center_x
    print "Y: %f" % final_rotation_center_y
    print "Translation X = %f" % final_translation_x
    print "Translation Y = %f" % final_translation_y
    print "Iterations = %f" % number_of_iterations
    print "Metric value = %f" % best_value

    transform.SetParameters(final_parameters)

    # GET TRANSFORM
    # ========================================================================

    final_transform = itk.CenteredRigid2DTransform.New()
    final_transform.SetCenter(transform.GetCenter())
    final_transform.SetParameters(final_parameters)
    final_transform.SetFixedParameters(transform.GetFixedParameters())

    data_file.close()

    return final_transform

def itk_registration_similarity_2d(fixed_image, moving_image, options,
                                initial_transform=None):
    """
    A Python implementation for a Rigid Body 2D registration, utilizing
    ITK (www.itk.org) library functions.

    :param fixed_image:
                            The reference image. Must be an instance of
                            itk.Image class. Numpy arrays can be converted
                            to this format with ITK PyBuffer
    :param moving_image:
                            The image for which the spatial transform will
                            be calculated. Same requirements as above.
    :param options:
                            Options provided by the user via CLI or the
                            included GUI. See script_options.py.
    :param initial_transform:
                            Initial transform may be provided by the user
                            instead of calculating it with an initializer.
                            The transform must be of the type
                            itk.CenteredRigid2DTransform
    :return:
                            The final transform, an instance of
                            itk.CenteredSimilarity2DTransform
    """
    print 'Setting up registration job'

    # Setup tempfile for saving registration progress
    data_file_path = tempfile.mkdtemp('-supertomo.register')
    data_to_save = ('Metric', 'Rotation', 'CenterX', 'CenterY', 'X-translation',
                    'Y-translation')
    data_file_name = os.path.join(data_file_path, 'registration_data.txt')
    data_file = io.RowFile(data_file_name, data_to_save, append=False)
    data_file.comment('Registration Command: %s' % (' '.join(map(str, sys.argv))))

    # Check if type conversions are needed.
    if options.use_internal_type is True:
        image_type = options.internal_type+'2'
    elif 'viola-wells' in options.registration_method:

        print "Normalizing images for Viola-Wells"
        # Cast to floating point. Can be single or double precision, depending
        # on the setting
        image_type = options.internal_type+'2'
        fixed_image = itkutils.type_cast(
            fixed_image,
            options.image_type,
            image_type
        )
        moving_image = itkutils.type_cast(
            moving_image,
            options.image_type,
            image_type
        )
        # Normalize images
        fixed_image = itkutils.normalize_image_filter(
            fixed_image,
            image_type
        )
        moving_image = itkutils.normalize_image_filter(
            moving_image,
            image_type
        )
    else:
        image_type = options.image_type

    fixed_image_region = fixed_image.GetBufferedRegion()

    # REGISTRATION COMPONENTS SETUP
    # ========================================================================
    transform = itk.CenteredSimilarity2DTransform.New()
    optimizer = itk.RegularStepGradientDescentOptimizer.New()
    interpolator = getattr(itk.LinearInterpolateImageFunction,
                           image_type+'D').New()

    registration = getattr(itk.ImageRegistrationMethod,
                           image_type+image_type).New()

    if 'mattes' in options.registration_method:
        image_metric = getattr(itk.MattesMutualInformationImageToImageMetric,
                               image_type+image_type).New()
        image_metric.SetNumberOfHistogramBins(options.mattes_histogram_bins)
        image_metric.SetNumberOfSpatialSamples(options.mattes_spatial_samples)
    elif 'viola-wells' in options.registration_method:
        image_metric = getattr(itk.MutualInformationImageToImageMetric,
                               image_type+image_type).New()

        image_metric.SetFixedImageStandardDeviation(options.vw_fixed_sd)
        image_metric.SetMovingImageStandardDeviation(options.vw_moving_sd)

        # Calculate number of spatial samples as a fraction of total pixel count
        # of the fixed image
        pixels = fixed_image_region.GetNumberOfPixels()
        number_of_samples = int(options.vw_samples_multiplier*pixels)
        print number_of_samples
        image_metric.SetNumberOfSpatialSamples(number_of_samples)

        optimizer.MaximizeOn()

    elif 'normalized-correlation' in options.registration_method:
        image_metric = getattr(itk.NormalizedCorrelationImageToImageMetric,
                               image_type+image_type).New()
        image_metric.SetFixedImageRegion(fixed_image.GetBufferedRegion())

    elif 'mean-squared-difference' in options.registration_method:
        image_metric = getattr(itk.MeanSquaresImageToImageMetric,
                               image_type+image_type).New()
    else:
        print "Registration method %s not implemented" \
              % options.registration_method
        return None

    # REGISTRATION
    # ========================================================================
    registration.SetMetric(image_metric)
    registration.SetOptimizer(optimizer)
    registration.SetTransform(transform)
    registration.SetInterpolator(interpolator)

    registration.SetFixedImage(fixed_image)
    registration.SetMovingImage(moving_image)
    registration.SetFixedImageRegion(fixed_image.GetBufferedRegion())

    if initial_transform is None:
        print 'Calculating initial registration parameters'
        # INITIALIZER
        # ====================================================================
        init_type = 'CS2DTD'+image_type+image_type
        initializer = getattr(itk.CenteredTransformInitializer, init_type).New()

        initializer.SetTransform(transform)
        initializer.SetFixedImage(fixed_image)
        initializer.SetMovingImage(moving_image)

        initializer.MomentsOn()

        initializer.InitializeTransform()

        transform.SetAngle(double(options.set_rotation))
        transform.SetScale(double(options.set_scale))

        # INITIAL PARAMETERS
        # ====================================================================

        registration.SetInitialTransformParameters(transform.GetParameters())

    else:
        assert isinstance(initial_transform, itk.CenteredRigid2DTransform)
        registration.SetInitialTransformParameters(initial_transform.GetParameters())

    # OPTIMIZER
    # ========================================================================
    print 'Setting up optimizer'
    # optimizer scale
    translation_scale = 1.0/options.translation_scale

    optimizer_scales = itk.Array.D(transform.GetNumberOfParameters())
    optimizer_scales.SetElement(0, options.scaling_scale)
    optimizer_scales.SetElement(1, 1.0)
    optimizer_scales.SetElement(2, translation_scale)
    optimizer_scales.SetElement(3, translation_scale)
    optimizer_scales.SetElement(4, translation_scale)
    optimizer_scales.SetElement(5, translation_scale)


    optimizer.SetScales(double(optimizer_scales))

    optimizer.SetMaximumStepLength(double(options.max_step_length))
    optimizer.SetMinimumStepLength(double(options.min_step_length))
    optimizer.SetNumberOfIterations(options.registration_max_iterations)

    optimizer.SetRelaxationFactor(double(options.relaxation_factor))


    # OBSERVER
    # ========================================================================

    def iteration_update():

        current_parameter = transform.GetParameters()
        optimizer_value = optimizer.GetValue()
        if options.print_registration_progress:
            print "M: %f   P: %f %f %f %f %f %f" % (
                optimizer_value,
                current_parameter.GetElement(0),
                current_parameter.GetElement(1),
                current_parameter.GetElement(2),
                current_parameter.GetElement(3),
                current_parameter.GetElement(4),
                current_parameter.GetElement(5)
            )
        tmplist = list()
        tmplist.append(optimizer_value)
        for j in range(len(current_parameter)):
            tmplist.append(current_parameter.GetElement(j))
        data_file.write(tmplist)

    iteration_command = itk.PyCommand.New()
    iteration_command.SetCommandCallable(iteration_update)
    optimizer.AddObserver(itk.IterationEvent(), iteration_command)

    # START
    # ========================================================================

    print "Starting registration"
    registration.Update()

    # Get the final parameters of the transformation
    #
    final_parameters = registration.GetLastTransformParameters()

    final_scale = final_parameters[0]
    final_angle = final_parameters[1]
    final_angle_in_degrees = final_angle*180.0/pi
    final_rotation_center_x = final_parameters[2]
    final_rotation_center_y = final_parameters[3]
    final_translation_x = final_parameters[4]
    final_translation_y = final_parameters[5]

    number_of_iterations = optimizer.GetCurrentIteration()
    best_value = optimizer.GetValue()

    print "Final Registration Parameters "
    print "Final scale = %f" % final_scale
    print "Angle (degrees)  = %f" % final_angle_in_degrees
    print "Center of rotation:"
    print "X: %f" % final_rotation_center_x
    print "Y: %f" % final_rotation_center_y
    print "Translation X = %f" % final_translation_x
    print "Translation Y = %f" % final_translation_y
    print "Iterations = %f" % number_of_iterations
    print "Metric value = %f" % best_value

    transform.SetParameters(final_parameters)

    # GET TRANSFORM
    # ========================================================================

    final_transform = itk.CenteredSimilarity2DTransform.New()
    #final_transform.SetCenter(transform.GetCenter())
    final_transform.SetParameters(final_parameters)
    final_transform.SetFixedParameters(transform.GetFixedParameters())

    data_file.close()

    return final_transform
