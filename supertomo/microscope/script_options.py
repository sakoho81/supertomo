
from optparse import OptionGroup
from supertomo.script_options import set_formatter

__all__ = ['add_psflib_options', 'set_estimate_psf_options',
           'set_deconvolve_options','get_rltv_options_group',
           'set_deconvolve_with_sphere_options', 'set_fusion_options']


def set_clusters_options (parser):
    set_formatter(parser)
    parser.set_usage ('%prog [options] [ -i INPUT_PATH ]')
    parser.set_description('Find clusters in INPUT_PATH field.')
    parser.add_option ('--input-path','-i',
                       type = 'file', metavar='INPUT_PATH', dest='input_path',
                       help = 'Specify INPUT_PATH.'
                       )
    parser.add_option ('--output-path','-o',
                       type = 'file', metavar='OUTPUT_PATH', dest='output_path',
                       help = 'Specify OUTPUT_PATH.'
                       )
    parser.add_option ('--detail-size', dest='detail_size',type = 'float',
                       help = 'Specify typical size of a detail to be resolved, in micrometers.')
    from supertomo.ops.script_options import get_regress_options_group
    parser.add_option_group(get_regress_options_group (parser))
    from supertomo.ops.script_options import get_fft_options_group
    parser.add_option_group(get_fft_options_group (parser))

def add_psflib_options (parser):
    from ..io.io import get_psf_libs, psflib_dir
    parser.add_option ('--psf-lib',
                       choices = ['<select>'] + sorted(get_psf_libs().keys()),
                       help = 'Select PSF library name (psflib_dir=%r).'%psflib_dir \
                           + ' Note that specifying --psf-path|--kernel-path options override this selection.'
                       )

def set_estimate_psf_options (parser):
    set_formatter(parser)
    parser.set_usage('%prog [options] [ [-i] INPUT_PATH [ [-o] OUTPUT_PATH ] ]')
    parser.set_description('''Find PSF estimate from the measurments of microspheres.
                              Intermediate results are saved to INPUT_PATH/supertomo.estimate_psf/
                              directory and estimated PSF is saved to OUTPUT_PATH
                           ''')
    parser.add_option ("--measurement-path", '--input-path','-i',
                       type = 'file', metavar='INPUT_PATH', dest='input_path',
                       help = '''Specify PATH to microsphere measurments.\
                                 To select directory PATH, find a file PATH/{PATHINFO.txt, configuration.txt,
                                 SCANINFO.txt} and select it.
                                 ''')

    parser.add_option ("--psf-path", '--output-path','-o','-k',
                       type = 'file', metavar='OUTPUT_PATH', dest='output_path',
                       help = '''Specify PATH for saving estimated PSF.'''
                       )
    #parser.add_option('--photon-counter-offset',
    #                  type = 'float',
    #                  help = 'Specify photon counter offset.'
    #                  )
    parser.add_option('--subtract-background-field', dest='subtract_background_field',
                      action = 'store_true',
                      help='Specify that subtraction of a background field should be carried out.')

    parser.add_option('--no-subtract-background-field', dest='subtract_background_field',
                      action = 'store_false',
                      help='See ``--subtract-background-field option``.')

    parser.add_option ("--cluster-background-level",
                       dest = 'cluster_background_level',
                       type = 'float',  metavar='FLOAT',
                       help = """ Specify maximum background level for defining PSF clusters: smaller
                                  value means larger clusters (a good thing) and higher probability of
                                  overlapping clusters (a bad thing); default will be estimated.
                                  """)

    parser.add_option ("--psf-field-size",
                       dest = 'psf_field_size',
                       type = 'float', default=7, metavar='FLOAT',
                       help = 'Specify PSF field size in lateral and axial resolution units.'
                       )

    parser.add_option ("--show-plots",
                       dest = 'show_plots', action = 'store_true',
                       help = 'Show estimated PSF during computation.',
                       )
    parser.add_option ("--no-show-plots",
                       dest = 'show_plots', action = 'store_false',
                       help = 'See ``--show-plot option``.',
                       )

    parser.add_option ("--save-intermediate-results",
                       dest = 'save_intermediate_results', action = 'store_true',
                       help = 'Save intermediate results of computation.',
                       )
    parser.add_option ('--no-save-intermediate-results',
                       dest = 'save_intermediate_results', action = 'store_false',
                       help = 'See ``--save-intermediate-results`` option.'
                       )
    parser.add_option ('--select-candidates',
                       help = 'Specify which canditates are selected for computing the PSF estimate.')

    from ..io.script_options import get_io_options_group, get_microscope_options_group
    parser.add_option_group(get_io_options_group (parser))
    parser.add_option_group(get_microscope_options_group (parser))


def set_deconvolve_options(parser):
    set_formatter(parser)
    parser.set_usage('%prog [options] [ [-k] PSF_PATH [-i] INPUT_PATH [ [-o] OUTPUT_PATH] ]')
    parser.set_description('Deconvolve INPUT_PATH with PSF_PATH.')
    add_psflib_options(parser)
    parser.add_option('--psf-path','-k', dest = 'psf_path',
                      type = 'file', metavar='PATH',
                      help = 'Specify PATH to PSF 3D images.'
                      )
    parser.add_option ('--input-path', '-i',
                       type = 'file', metavar='PATH',
                       help = 'Specify input PATH of 3D images.'
                       )
    parser.add_option ('--output-path','-o',
                       type = 'file', metavar='PATH',
                       help = 'Specify output PATH of 3D images.'
                       )
    parser.add_option ('--max-nof-iterations',
                       type = 'int', default=10,
                       help = 'Specify maximum number of iterations.')
    parser.add_option('--convergence-epsilon',
                      type = 'float', default=0.05,
                      help = 'Specify small positive number that determines the window for convergence criteria.')
    parser.add_option('--degrade-input', action='store_true',
                      help = 'Degrade input: apply noise to convolved input.')
    parser.add_option('--no-degrade-input', action='store_false', dest='degrade_input',
                      help = 'See ``--degrade-input``.')
    parser.add_option('--degrade-input-snr',
                      type = 'float', default=0.0,
                      help = 'Specify the signal-to-noise ratio when using --degrade-input.'\
                          'If set to 0, then snr will be estimated as sqrt(max(input image)).')
    parser.add_option ('--first-estimate',
                      choices = ['input image',
                                 'convolved input image',
                                 '2x convolved input image',
                                 'last result'
                                 ],
                      help = 'Specify first estimate for iteration.')
    parser.add_option('--save-intermediate-results',
                      action = 'store_true',
                      help = 'Save intermediate results.')
    parser.add_option('--no-save-intermediate-results',
                      dest = 'save_intermediate_results',
                      action = 'store_false',
                      help = 'See ``--save-intermediate-results`` option.')
    from ..ops.script_options import get_apply_window_options_group
    parser.add_option_group(get_apply_window_options_group (parser))
    parser.add_option_group(get_rltv_options_group (parser))
    from ..ops.script_options import get_fft_options_group
    parser.add_option_group(get_fft_options_group (parser))
    from ..script_options import get_runner_options_group
    parser.add_option_group(get_runner_options_group (parser))
    from ..io.script_options import get_microscope_options_group
    parser.add_option_group(get_microscope_options_group (parser))

def get_rltv_options_group(parser):
    group = OptionGroup (parser, 'Richardson-Lucy algorithm options',
                         description = '''Specify options for Richardson-Lucy deconvolution
                                          algorithm with total variation term.
                                       ''')
    group.add_option ('--rltv-estimate-lambda', dest='rltv_estimate_lambda',
                      action='store_true',
                      help = 'Enable estimating RLTV parameter lambda.')
    group.add_option ('--no-rltv-estimate-lambda',
                      dest='rltv_estimate_lambda', action='store_false',
                      help = 'See ``--rltv-estimate-lambda`` option.')
    group.add_option ('--rltv-lambda-lsq-coeff',
                      type = 'float',
                      default = 0.0,
                      help = 'Specify coefficient for RLTV regularization parameter. If set '
                             'to 0 then the coefficent will be chosed such that lambda_lsq_0==50/SNR.'
                             '')
    group.add_option ('--rltv-lambda',
                      type = 'float',
                      default = 0.0,
                      help = 'Specify RLTV regularization parameter.')
    group.add_option ('--rltv-compute-lambda-lsq',
                      action='store_true',
                      help = 'Compute RLTV parameter estimation lambda_lsq.')
    group.add_option ('--rltv-algorithm-type',
                      choices = ['multiplicative', 'additive'], default='multiplicative',
                      help = 'Specify algorithm type. Use multiplicative with Poisson noise '
                             'and additive with Gaussian noise.')
    group.add_option ('--rltv-alpha',
                      type = 'float',
                      help = 'Specify additive RLTV regularization parameter.')
    group.add_option ('--rltv-stop-tau',
                      type = 'float',
                      default = 0.002,
                      help = 'Specify parameter for tau-stopping criteria.')
    return group

def set_deconvolve_with_sphere_options (parser):
    set_formatter(parser)
    parser.set_usage('%prog [options] [ [-i] INPUT_PATH [ [-o] OUTPUT_PATH ] ]')
    parser.set_description('Deconvolve INPUT_PATH with sphere.')
    parser.add_option ('--sphere-diameter', dest='diameter',
                       type = 'float',
                       default = 170,
                       help = 'Specify sphere diameter in nanometers.')
    parser.add_option ('--input-path', '-i',
                       type = 'file', metavar='PATH',
                       help = 'Specify input PATH of 3D images.'
                       )
    parser.add_option ('--output-path','-o',
                       type = 'file', metavar='PATH',
                       help = 'Specify output PATH of 3D images.'
                       )
    parser.add_option ('--max-nof-iterations',
                       type = 'int', default=10,
                       help = 'Specify maximum number of iterations.')
    parser.add_option('--convergence-epsilon',
                      type = 'float', default=0.05,
                      help = 'Specify small positive number that determines '
                             'the window for convergence criteria.')

    from ..ops.script_options import get_fft_options_group
    from ..io.script_options import get_microscope_options_group
    parser.add_option_group(get_fft_options_group (parser))
    parser.add_option_group(get_rltv_options_group (parser))
    parser.add_option_group(get_microscope_options_group (parser))


def set_fusion_options(parser):
    set_formatter(parser)
    parser.set_usage('%prog [-register] [-fuse] [ [-f] FIXED_IMAGE_PATH [-m] '
                     'MOVING_IMAGE_PATH [-p] PSF [ [-o] OUTPUT_PATH] ] [options]')
    parser.set_description('Register and/or Fuse FIXED_IMAGE with MOVING_IMAGE')

    parser.add_option(
        '--verbose',
        action='store_true'
    )
    parser.add_option(
        '--hide-warnings',
        dest='hide_warnings',
        action='store_true',
        help='Hide ITK wrapper warnings'
    )

    parser.add_option(
        '--internal-type',
        dest='intern IMal_type',
        choices=['IUC', 'IF', 'ID'],
        default='IF',
        help='Specify the internal pixel type to be used in image'
             'registration'
    )
    parser.add_option(
        '--prefix',
        dest='path_prefix',
        default='/home/sami/Pictures/SuperTomo',
        help='Path to image files'
    )
    parser.add_option(
        '--register',
        action='store_true',
        dest='registration',
        help='Registration on/off',
        default=False
    )
    parser.add_option(
        '--fuse',
        action='store_true',
        dest='fusion',
        help='Fusion on/off',
        default=False
    )
    parser.add_option(
        '--fixed', '-f',
        dest='fixed_image_path',
        metavar='PATH',
        help='Specify PATH to Fixed Image'
    )
    parser.add_option(
        '--moving', '-m',
        dest='moving_image_path',
        metavar='PATH',
        help='Specify PATH to Moving Image'
    )

    parser.add_option(
        '--psf', '-p',
        dest='psf_path',
        metavar='PATH',
        help='Specify PATH to PSF images.'
    )
    parser.add_option(
        '--psf-type',
        dest='psf_type',
        choices=['single', 'measured'],
        default='single',
        help='Define a PSF to be used in the fusion'
    )
    parser.add_option(
        '--transform',
        action='store_true',
        help='Transform with pre-defined parameters (in a file)'
    )
    parser.add_option(
        '--transform-path', '-t',
        dest='transform_path',
        metavar='PATH',
        help='Specify PATH to transform file'
    )
    parser.add_option(
        '--max-nof-iterations',
        type='int',
        default=100,
        help='Specify maximum number of iterations.'
    )
    parser.add_option(
        '--convergence-epsilon',
        type='float',
        default=0.05,
        help='Specify small positive number that determines '
             'the window for convergence criteria.'
    )
    parser.add_option(
        '--degrade-input',
        action='store_true',
        default=False,
        help='Degrade input: apply noise to convolved input.'
    )
    parser.add_option(
        '--degrade-input-snr',
        type='float',
        default=0.0,
        help='Specify the signal-to-noise ratio when using --degrade-input.'
             'If set to 0, snr will be estimated as sqrt(max(input image)).'
    )
    parser.add_option(
        '--first-estimate',
        choices=['input image',
                 'convolved input image',
                 'sum of all projections',
                 'stupid tomo',
                 '2x convolved input image',
                 'last result',
                 'image_mean',
                 'average'],
        default='image_mean',
        help='Specify first estimate for iteration.'
    )
    parser.add_option(
        '--save-intermediate-results',
        action='store_true',
        help='Save intermediate results.'
    )
    parser.add_option(
        '--no-save-intermediate-results',
        dest='save_intermediate_results',
        action='store_false',
        help='See ``--save-intermediate-results`` option.'
    )
    parser.add_option(
        '--show-plots',
        dest='show_plots',
        action='store_true',
        help='Show summary plots of registration/fusion variables'
    )
    parser.add_option(
        '--show-image',
        dest='show_image',
        action='store_true',
        help='Show a 3D image of the fusion/registration result upon '
             'completion'
    )

    parser.add_option(
        '--set-3d-rendering',
        dest='rendering_method',
        choices=['volume', 'surface'],
        default='volume',
        help='Set the rendering method to be used for 3d data plots'
    )

    parser.add_option(
        '--subset',
        dest='subset_image',
        type='float',
        default=1.0,
        help="If you would like to work with a subset of data, instead of"
             "the whole image, specify a multiplier ]0, 1[ here"
    )
    parser.add_option(
        '--rescale',
        dest='rescale_to_full_range',
        action='store_true',
        help='Rescale intensities of the input images to the full dynamic'\
             'range allowed by the pixel type'
    )

    parser.add_option(
        '--output-cast',
        dest='output_cast',
        action='store_true',
        help='By default the fusion output is returned as a 32-bit image'
             'This switch can be used to enbale 8-bit unsigned output'
    )
    parser.add_option(
        '--fusion-method',
        dest='fusion_method',
        choices=['multiplicative', 'multiplicative-opt', 'summative', 'summative-opt'],
        default='summative'
    )

    # Add options for Windowing
    from ..ops.script_options import get_apply_window_options_group
    parser.add_option_group(get_apply_window_options_group(parser))
    # Add options for RLTV regularization
    parser.add_option_group(get_rltv_options_group(parser))
    # Add options for Fast Fourier Transform control
    from ..ops.script_options import get_fft_options_group
    parser.add_option_group(get_fft_options_group(parser))
    # Add microscope options
    from ..io.script_options import get_microscope_options_group
    parser.add_option_group(get_microscope_options_group(parser))
    # Add ITK specific options
    from ..io.script_options import get_itk_io_options
    parser.add_option_group(get_itk_io_options(parser))
    # Add Options for Mattes Registration
    from .registration import get_registration_options_group
    get_registration_options_group(parser)
