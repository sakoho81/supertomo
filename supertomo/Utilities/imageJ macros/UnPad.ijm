getDimensions(width, height, channels, slices, frames);

if (slices == 1 && frames > slices) {
	run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
	run("Hyperstack to Stack");
	slices = frames;
}

Dialog.create("Real Dimensions");
Dialog.addNumber("X:", width);
Dialog.addNumber("Y:", height);
Dialog.addNumber("Z:", slices);
Dialog.show();
	
x_real = Dialog.getNumber();
y_real = Dialog.getNumber();
z_real = Dialog.getNumber();
	
x_diff = width - x_real;
y_diff = height - y_real;
z_diff = slices - z_real;
	
if (x_diff > 0 || y_diff > 0)
{
	run("Specify...", "width="+x_real+" height="+y_real+" x="+x_diff/2-1+" y="+y_diff/2-1);
	run("Crop");
}
if (z_diff > 0) 
{
	start = z_diff/2-1;
	end = start + z_real -1;
	run("Make Substack...", "slices="+start+"-"+end);
}
