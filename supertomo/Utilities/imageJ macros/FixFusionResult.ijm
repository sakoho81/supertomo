
// Open file
path = File.openDialog("Select a SuperTomo TIFF File");
open(path);

// Set pixel size
properties = getMetadata("Info"); 
List.setList(properties); 
run("Set Scale...", "distance=1 known="+List.get("VoxelSizeX")+" pixel=1 unit=um");

// Reorder Hyperstack. For some reason these go wrong when opening in ImageJ. 
run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");

// Set LUT
run("Red Hot"); 
