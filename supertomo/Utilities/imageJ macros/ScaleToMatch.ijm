

// Get reference and target image titles
allTitles=newArray(nImages); 
nStacks=0; 
for (i=1; i<=nImages; i++) { 
	selectImage(i); 
	allTitles[i-1]=getTitle(); 
} 
 
Dialog.create("Select images for rescaling"); 
Dialog.addChoice("Reference", allTitles);
Dialog.addChoice("Target", allTitles); 
Dialog.show(); 

reference = Dialog.getChoice();
target= Dialog.getChoice();

// Get reference image information
selectImage(reference);
getVoxelSize(rWidth, rHeight, rDepth, rUnit);
rSlices = nSlices;

// Get target image information
selectImage (target);
getVoxelSize(tWidth, tHeight, tDepth, tUnit);
tSlices = nSlices;

// Rescale
if (rSlices > 1 && tSlices > 1) {
	
	run("TransformJ Scale", "x-factor="+tWidth/rWidth
	+" y-factor="+tHeight/rHeight
	+" z-factor="+tDepth/rDepth
	+" interpolation=[quintic B-spline]");

} else if (rSlices == 1 && tSlices == 1) {
	run("TransformJ Scale", "x-factor="+tWidth/rWidth
	+" y-factor="+tHeight/rHeight
	+" z-factor=1.0"
	+" interpolation=[quintic B-spline]");
} else {
	print ("The dimensions of the selected images do not match");
}