// Sami Koho University of Turku 2013-05-14
// This is a simple macro for rescaling an image stack so that pixel size in all three dimensions
// is equal. Pixel size information has to be available in order for this to work.

getVoxelSize(width, height, depth, unit);
scaling = depth/width;
run("TransformJ Scale", "x-factor=1.0 y-factor=1.0 z-factor="+scaling
+" interpolation=[quintic B-spline]");

