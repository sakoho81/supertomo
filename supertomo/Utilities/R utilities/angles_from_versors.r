#!/usr/bin/env Rscript

# A tiny utility, written in R to calculate rotation angles
# and axes on a data-file containing XYZ versors. 
# The calculated values will be added to the original data
# and saved to angles.csv file

data <- read.table ("versors.csv", header=TRUE, dec=".", sep=",")
K <- sqrt(data$X^2+data$Y^2+data$Z^2)
data$angle = 2.0 * asin(K)
data$axisX <- data$X/K
data$axisY <- data$Y/K
data$axisZ <- data$Z/K

write.table(data, file="angles.csv", sep=",")