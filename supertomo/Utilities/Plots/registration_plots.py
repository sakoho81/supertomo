import sys
import os
import numpy
from iocbio.io import io
import matplotlib as mpl
from matplotlib import pyplot as plt
from matplotlib import cm, rcParams


def plot_rowfile(path, shape=330):

    data, titles = io.RowFile(path).read(with_titles=True)

    lst = map(int, str(shape))
    assert len(titles) <= lst[0]*lst[1]

    colors = cm.rainbow(numpy.linspace(0, 1, len(titles)))

    plot = 0
    for i, j in zip(titles, colors):
        temp = data[i]
        plt.subplot(shape+plot)
        plt.plot(numpy.arange(len(temp)), numpy.array(temp), linewidth=1.5,
                 color=j)
        plt.title(i)
        plot += 1

    plt.tight_layout()
    plt.show()

def plot_single_from_rowfile(path, varname, plot_title="", y_label="",
                             x_label=""):

    data, titles = io.RowFile(path).read(with_titles=True)

    assert varname in titles

    rcParams.update({'font.size': 14})
    rcParams.update({'font.family': 'sans.serif'})
    rcParams.update({'font.sans-serif': 'Arial'})


    yvals = data[varname]
    xvals = numpy.arange(len(yvals))

    plt.plot(
        xvals,
        yvals,
        linewidth=2,
        color='r',
        label=varname
    )
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.title(plot_title)

    #plt.legend(loc=1)

    plt.show()

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print "You must specify a RowFile"
        sys.exit(1)
    path = sys.argv[1]
    assert os.path.isfile(path)

    data, titles = io.RowFile(path).read(with_titles=True)

    font = {'family': 'sans-serif',
            'size': 14}

    mpl.rc('font', **font)

    yvals = (data['Metric'],
             data['X-versor'],
             data['Y-versor'],
             data['Z-versor'],
             data['X-translation'],
             data['Y-translation'],
             data['Z-translation'])
    xvals = numpy.arange(len(yvals[0]))
    y_label = ('Mutual Information',
               'Versor',
               'Versor',
               'Versor',
               'Translation (um)',
               'Translation (um)',
               'Translation (um)')
    colors = cm.rainbow(numpy.linspace(0, 1, len(titles)))

    for i in range(len(yvals)):
        plt.subplot(3, 3, i+3)
        plt.plot(xvals, yvals[i], linewidth=2, color=colors[i])
        plt.xlabel('Iteration')
        plt.ylabel(y_label[i])

    plt.tight_layout(pad=0.2)
    plt.show()


