import sys
import os
from numpy import arange
import matplotlib as mpl
from matplotlib import pyplot as plt


from iocbio.io import io

if len(sys.argv) < 2:
    print "You must specify a RowFile"
    sys.exit(1)
path = sys.argv[1]
assert os.path.exists(path)

font = {'family': 'sans-serif',
            'size': 14}
mpl.rc('font', **font)

if os.path.isfile(path):

    data, titles = io.RowFile(path).read(with_titles=True)
    yvals = (data['u_esu'], data['tau1'])
    xvals = arange(len(yvals[0]))
    y_label = ('Non converging', 'Relative change')
    for i in range(len(yvals)):
        plt.subplot(1, 2, i)
        plt.plot(xvals, yvals[i], linewidth=2)
        plt.yscale('log')
        plt.xlabel('Iteration')
        plt.ylabel(y_label[i])

    plt.tight_layout()
    plt.show()

else:
    for filename in os.listdir(path):
        if "fusion_data" in filename and filename.endswith(".txt"):
            real_path = os.path.join(path, filename)
            data, titles = io.RowFile(real_path).read(with_titles=True)
            yvals = (data['u_esu'], data['tau1'])
            xvals = arange(len(yvals[0]))
            y_label = ('Non converging', 'Relative change')
            for i in range(len(yvals)):
                plt.subplot(1, 2, i)
                plt.plot(xvals, yvals[i], linewidth=2)
                plt.yscale('log')
                plt.xlabel('Iteration')
                plt.ylabel(y_label[i])

    plt.tight_layout()
    plt.show()

