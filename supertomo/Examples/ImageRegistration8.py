__author__ = 'Sami Koho'


import itk
import sys
import matplotlib.pyplot as plt
from math import pi

# Globals

observerData = []


numberOfBins = 24
numberOfSamples = 10000

# Check that there is the right amount of arguments. There should be two
# input images and an output image
if len(sys.argv) < 4:
    print >> sys.stderr, "Missing Parameters "
    print "Usage: {0}".format(sys.argv[0])
    print "fixedImageFile  movingImageFile outputImagefile [differenceAfterRegistration]"
    print "[differenceBeforeRegistration] [sliceBeforeRegistration]"
    print "[sliceDifferenceBeforeRegistration] [sliceDifferenceAfterRegistration]"
    print "[sliceAfterRegistration]"
    sys.exit(1)

# READ INPUT IMAGES
# ==========================================================================================================
fixedImageReader = itk.ImageFileReader.IF3.New()
movingImageReader = itk.ImageFileReader.IF3.New()

fixedImageReader.SetFileName(sys.argv[1])
movingImageReader.SetFileName(sys.argv[2])

fixedImageReader.Update()
movingImageReader.Update()

fixedImage = fixedImageReader.GetOutput()
movingImage = movingImageReader.GetOutput()

# SETUP REGISTRATION JOB
# ==========================================================================================================
registration = itk.ImageRegistrationMethod.IF3IF3.New()
imageMetric = itk.MeanSquaresImageToImageMetric.IF3IF3.New()
transform = itk.VersorRigid3DTransform.D.New()
optimizer = itk.VersorRigid3DTransformOptimizer.New()
interpolator = itk.LinearInterpolateImageFunction.IF3D.New()


# REGISTRATION
# ==========================================================================================================
registration.SetMetric(imageMetric)
registration.SetOptimizer(optimizer)
registration.SetTransform(transform)
registration.SetInterpolator(interpolator)

registration.SetFixedImage(fixedImage)
registration.SetMovingImage(movingImage)

registration.SetFixedImageRegion(fixedImage.GetBufferedRegion())

# INITIALIZER
# ==========================================================================================================
initializer = itk.CenteredTransformInitializer.VR3DTDIF3IF3.New()
initializer.SetTransform(transform)
initializer.SetFixedImage(fixedImage)
initializer.SetMovingImage(movingImage)

initializer.MomentsOn()

initializer.InitializeTransform()


# INITIAL PARAMETERS
# ==========================================================================================================

# Initialize rotation
rotation = transform.GetVersor()
axis = rotation.GetAxis()

axis[0] = 0.0
axis[1] = 0.0
axis[2] = 1.0

rotation.Set(axis, 0)

transform.SetRotation(rotation)

# Set initial parameters. The initializer will communicate directly with transform
registration.SetInitialTransformParameters(transform.GetParameters())

# OPTIMIZER
# ==========================================================================================================

# optimizer scale
translationScale = 1.0 / 1000.0

optimizerScales = itk.Array.D(transform.GetNumberOfParameters())
optimizerScales.SetElement(0, 1.0)
optimizerScales.SetElement(1, 1.0)
optimizerScales.SetElement(2, 1.0)
optimizerScales.SetElement(3, translationScale)
optimizerScales.SetElement(4, translationScale)
optimizerScales.SetElement(5, translationScale)

optimizer.SetScales(optimizerScales)

optimizer.SetMaximumStepLength(0.2)
optimizer.SetMinimumStepLength(0.0001)
optimizer.SetNumberOfIterations(200)

# OBSERVER
# ==========================================================================================================

def iterationUpdate():

    currentParameter = transform.GetParameters()
    temp = optimizer.GetValue()
    print "M: %f   P: %f %f %f %f %f %f" % (temp,
                        currentParameter.GetElement(0),
                        currentParameter.GetElement(1),
                        currentParameter.GetElement(2),
                        currentParameter.GetElement(3),
                        currentParameter.GetElement(4),
                        currentParameter.GetElement(5))
    observerData.append(temp)

iterationCommand = itk.PyCommand.New()
iterationCommand.SetCommandCallable(iterationUpdate)
optimizer.AddObserver(itk.IterationEvent(), iterationCommand)

# START
# ==========================================================================================================

print "Starting registration"
registration.Update()

# Get the final parameters of the transformation
#
finalParameters = registration.GetLastTransformParameters()

versorX = finalParameters[0]
versorY = finalParameters[1]
versorZ = finalParameters[2]
finalTranslationX = finalParameters[3]
finalTranslationY = finalParameters[4]
finalTranslationZ = finalParameters[5]

numberOfIterations = optimizer.GetCurrentIteration()
bestValue = optimizer.GetValue()

print "Final Registration Parameters "
print "Versor X  = %f" % versorX
print "Versor Y = %f" % versorY
print "Versor Z = %f" % versorZ
print "Translation X = %f" % finalTranslationX
print "Translation Y = %f" % finalTranslationY
print "Translation Z = %f" % finalTranslationZ
print "Iterations = %f" % numberOfIterations
print "Metric value = %f" % bestValue


transform.SetParameters(finalParameters)
matrix = transform.GetMatrix()
offset = transform.GetOffset()

print "Matrix = "
for i in range(4):
    print "%f %f %f %f" % (matrix(i,0), matrix(i,1), matrix(i,2), matrix(i,3))

print "Offset = "
print offset


# RESAMPLE AND SAVE
# ==========================================================================================================

resampler = itk.ResampleImageFilter.IF3IF3.New()

finalTrasform = itk.VersorRigid3DTransform.D.New()
finalTrasform.SetCenter(transform.GetCenter())
finalTrasform.SetParameters(finalParameters)
finalTrasform.SetFixedParameters(transform.GetFixedParameters())

resampler.SetTransform(finalTrasform)
resampler.SetInput(movingImage)
region = fixedImage.GetLargestPossibleRegion()
resampler.SetSize(region.GetSize())
resampler.SetOutputOrigin(fixedImage.GetOrigin())
resampler.SetOutputSpacing(fixedImage.GetSpacing())
resampler.SetOutputDirection(fixedImage.GetDirection())
resampler.SetDefaultPixelValue(100)

outputCast = itk.CastImageFilter.IF3IUC3.New()
outputCast.SetInput(resampler.GetOutput())

#  Write the resampled image#
writer = itk.ImageFileWriter.IUC3.New()
writer.SetInput(outputCast.GetOutput())
writer.SetFileName(sys.argv[3])
writer.Update()

if len(sys.argv) > 4:
    difference = itk.SubtractImageFilter.IF3IF3IF3.New()
    difference.SetInput1(fixedImageReader.GetOutput())
    difference.SetInput2(resampler.GetOutput())

    resampler.SetDefaultPixelValue(1)

    intensityRescaler = itk.RescaleIntensityImageFilter.IF3IUC3.New()
    intensityRescaler.SetOutputMinimum(0)
    intensityRescaler.SetOutputMaximum(255)
    intensityRescaler.SetInput(difference.GetOutput())

    writer.SetFileName(sys.argv[4])
    writer.SetInput(intensityRescaler.GetOutput())
    writer.Update()

if len(sys.argv) > 5:
    identityTransform = itk.CenteredRigid3DTransform.New()
    identityTransform.SetIdentity()
    resampler.SetTransform(identityTransform)
    writer.SetFileName(sys.argv[5])
    writer.Update()

if len(sys.argv) > 6:
    extractor = itk.ExtractImageFilter.IUC3IUC2.New()
    extractor.SetDirectionCollapseToSubmatrix()
    extractor.InPlaceOn()

    inputRegion = fixedImage.GetLargestPossibleRegion()
    size = inputRegion.GetSize()
    start = inputRegion.GetIndex()

    size[2] = 0
    start[2] = 90

    desiredRegion = fixedImage.GetLargestPossibleRegion()
    desiredRegion.SetSize(size)
    desiredRegion.SetIndex(start)

    extractor.SetExtractorRegion(desiredRegion)

    sliceWriter = itk.ImageFileWriter.IUC2.New()
    sliceWriter.SetInput(extractor.GetOutput())

    extractor.SetInput(outputCast.GetOutput())
    resampler.SetTransform(identityTransform)
    sliceWriter.SetFilename(sys.argv[6])
    sliceWriter.Update()

if len(sys.argv) > 7:
    extractor.SetInput(intensityRescaler.GetOutput())
    resampler.SetTransform(identityTransform)
    sliceWriter.SetFilename(sys.argv[7])
    sliceWriter.Update()

if len(sys.argv) > 8:
    resampler.SetTransform(finalTrasform)
    sliceWriter.SetFilename(sys.argv[8])
    sliceWriter.Update()

if len(sys.argv) > 9:
    extractor.SetInput(outputCast.GetOutput())
    resampler.SetTransform(finalTrasform)
    sliceWriter.SetFilename(sys.argv[9])
    sliceWriter.Update()

