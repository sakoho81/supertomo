__author__ = 'Sami Koho'


import itk
import sys
import matplotlib.pyplot as plt
from math import pi

# Globals

observerData = []


numberOfBins = 24
numberOfSamples = 10000

# Check that there is the right amount of arguments. There should be two
# input images and an output image
if len(sys.argv) < 4:
    print >> sys.stderr, "Missing Parameters "
    print "Usage: {0}".format(sys.argv[0])
    print "fixedImageFile  movingImageFile outputImagefile [differenceAfterRegistration]"
    print "differenceBeforeRegistration]"
    sys.exit(1)

# READ INPUT IMAGES
# ==========================================================================================================
fixedImageReader = itk.ImageFileReader.IF2.New()
movingImageReader = itk.ImageFileReader.IF2.New()

fixedImageReader.SetFileName(sys.argv[1])
movingImageReader.SetFileName(sys.argv[2])

fixedImageReader.Update()
movingImageReader.Update()

fixedImage = fixedImageReader.GetOutput()
movingImage = movingImageReader.GetOutput()

# SETUP REGISTRATION JOB
# ==========================================================================================================
registration = itk.ImageRegistrationMethod.IF2IF2.New()
imageMetric = itk.MeanSquaresImageToImageMetric.IF2IF2.New()
transform = itk.CenteredRigid2DTransform.New()
optimizer = itk.RegularStepGradientDescentOptimizer.New()
interpolator = itk.LinearInterpolateImageFunction.IF2D.New()


# REGISTRATION
# ==========================================================================================================
registration.SetMetric(imageMetric)
registration.SetOptimizer(optimizer)
registration.SetTransform(transform)
registration.SetInterpolator(interpolator)

registration.SetFixedImage(fixedImage)
registration.SetMovingImage(movingImage)

registration.SetFixedImageRegion(fixedImage.GetBufferedRegion())

# INITIALIZER
# !!! This does not currently work as the initializer has been templated for 3D transforms
# in python only !!
# ==========================================================================================================
initializer = itk.CenteredTransformInitializer.New()
initializer.SetTransform(transform)
initializer.SetFixedImage(fixedImage)
initializer.SetMovingImage(movingImage)

initializer.MomentsOn()

initializer.InitializeTransform()

# INITIAL PARAMETERS
# ==========================================================================================================
registration.SetInitialTransformParameters(transform.GetParameters())

# OPTIMIZER
# ==========================================================================================================

# optimizer scale
translationScale = 1.0 / 1000.0

optimizerScales = itk.Array.D(transform.GetNumberOfParameters())
optimizerScales.SetElement(0, 1.0)
optimizerScales.SetElement(1, translationScale)
optimizerScales.SetElement(2, translationScale)
optimizerScales.SetElement(3, translationScale)
optimizerScales.SetElement(4, translationScale)

optimizer.SetScales(optimizerScales)

optimizer.SetMaximumStepLength(0.1)
optimizer.SetMinimumStepLength(0.001)
optimizer.SetNumberOfIterations(200)

# OBSERVER
# ==========================================================================================================

def iterationUpdate():

    currentParameter = transform.GetParameters()
    temp = optimizer.GetValue()
    print "M: %f   P: %f %f %f %f %f" % (temp,
                        currentParameter.GetElement(0),
                        currentParameter.GetElement(1),
                        currentParameter.GetElement(2),
                        currentParameter.GetElement(3),
                        currentParameter.GetElement(4))
    observerData.append(temp)

iterationCommand = itk.PyCommand.New()
iterationCommand.SetCommandCallable(iterationUpdate)
optimizer.AddObserver(itk.IterationEvent(), iterationCommand)

# START
# ==========================================================================================================

print "Starting registration"
registration.Update()

# Get the final parameters of the transformation
#
finalParameters = registration.GetLastTransformParameters()

finalAngle = finalParameters[0]
finalAngleInDegrees = finalAngle*180/pi
finalRotationCenterX = finalParameters[1]
finalRotationCenterY = finalParameters[2]
finalTranslationX = finalParameters[3]
finalTranslationY = finalParameters[4]

numberOfIterations = optimizer.GetCurrentIteration()
bestValue = optimizer.GetValue()

print "Final Registration Parameters "
print "Angle (radians)  = %f" % finalAngle
print "Angle (degrees) = %f" % finalAngleInDegrees
print "Rotation Center X = %f" % finalRotationCenterX
print "Rotation Center Y = %f" % finalRotationCenterY
print "Translation in  X = %f" % finalTranslationX
print "Translation in  Y = %f" % finalTranslationY
print "Iterations = %f" % numberOfIterations
print "Metric value = %f" % bestValue


# RESAMPLE AND SAVE
# ==========================================================================================================


# Resample the moving image to 2D 8bit format
resampler = itk.ResampleImageFilter.IF2IF2.New()
resampler.SetTransform(transform)
resampler.SetInput(movingImage)

region = fixedImage.GetLargestPossibleRegion()

resampler.SetSize(region.GetSize())

resampler.SetOutputSpacing(fixedImage.GetSpacing())
resampler.SetOutputOrigin(fixedImage.GetOrigin())
resampler.SetOutputDirection(fixedImage.GetDirection())
resampler.SetDefaultPixelValue(100)

outputCast = itk.RescaleIntensityImageFilter.IF2IUC2.New()
outputCast.SetOutputMinimum(0)
outputCast.SetOutputMaximum(255)
outputCast.SetInput(resampler.GetOutput())

#  Write the resampled image#
writer = itk.ImageFileWriter.IUC2.New()
writer.SetFileName(sys.argv[3])
writer.SetInput(outputCast.GetOutput())
writer.Update()

if len(sys.argv) > 4:
    difference = itk.SubtractImageFilter.IF2IF2IF2.New()
    difference.SetInput1(fixedImageReader.GetOutput())
    difference.SetInput2(resampler.GetOutput())

    resampler.SetDefaultPixelValue(1)

    intensityRescaler = itk.RescaleIntensityImageFilter.IF2IUC2.New()
    intensityRescaler.SetOutputMinimum(0)
    intensityRescaler.SetOutputMaximum(255)
    intensityRescaler.SetInput(difference.GetOutput())

    writer.SetFileName(sys.argv[4])
    writer.SetInput(intensityRescaler.GetOutput())
    writer.Update()

if len(sys.argv) > 5:
    identityTransform = itk.CenteredRigid2DTransform.New()
    identityTransform.SetIdentity()
    resampler.SetTransform(identityTransform)
    writer.SetFileName(sys.argv[5])
    writer.Update()


# Plot the registration progress.
# This would make more sense inside the observer
# function
plt.plot(observerData)
plt.ylabel("Optimizer value")
plt.show()
