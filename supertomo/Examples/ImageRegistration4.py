__author__ = 'Sami Koho'

import itk
import sys
import matplotlib.pyplot as plt
# Globals

observerData = []


numberOfBins = 24
numberOfSamples = 10000

# Check that there is the right amount of arguments. There should be two
# input images and an output image
if len(sys.argv) < 4:
    print >> sys.stderr, "Missing Parameters "
    print "Usage: {0}".format(sys.argv[0])
    print "fixedImageFile  movingImageFile outputImagefile [defaultPixelValue]"
    print "[checkerBoardAfter] [checkerBoardBefore] [numberOfBins] [numberOfSamples]"
    print "[useExplicitPDFderivatives ] "
    sys.exit(1)

# Get the two images to register
fixedImageReader = itk.ImageFileReader.IF2.New()
movingImageReader = itk.ImageFileReader.IF2.New()

fixedImageReader.SetFileName(sys.argv[1])
movingImageReader.SetFileName(sys.argv[2])

fixedImageReader.Update()
movingImageReader.Update()

fixedImage = fixedImageReader.GetOutput()
movingImage = movingImageReader.GetOutput()

# Setup registration components

registration = itk.ImageRegistrationMethod.IF2IF2.New()
imageMetric  = itk.MattesMutualInformationImageToImageMetric.IF2IF2.New()
transform = itk.TranslationTransform.D2.New()
optimizer = itk.RegularStepGradientDescentOptimizer.New()
interpolator = itk.LinearInterpolateImageFunction.IF2D.New()

# Registration
registration.SetMetric(imageMetric)
registration.SetOptimizer(optimizer)
registration.SetTransform(transform)
registration.SetInterpolator(interpolator)

registration.SetFixedImage(fixedImage)
registration.SetMovingImage(movingImage)

registration.SetFixedImageRegion(fixedImage.GetBufferedRegion())

# Image metric

if len(sys.argv) > 7:
    numberOfBins = int(sys.argv[7])

if len(sys.argv) > 8:
    numberOfBins = int(sys.argv[8])

imageMetric.SetNumberOfHistogramBins(numberOfBins)
imageMetric.SetNumberOfSpatialSamples(numberOfSamples)

if len(sys.argv) > 9:
    imageMetric.SetUseExplicitPDFDerivatives(int(sys.argv[9]))

# Initial parameters
initialParameters = transform.GetParameters()

initialParameters[0] = 0.0
initialParameters[1] = 0.0

registration.SetInitialTransformParameters(initialParameters)

#  Define optimizer parameters
#
optimizer.MinimizeOn()
optimizer.SetMaximumStepLength(4.00)
optimizer.SetMinimumStepLength(0.01)
optimizer.SetNumberOfIterations(200)

optimizer.SetRelaxationFactor(0.8)

# Iteration Observer
#
def iterationUpdate():

    currentParameter = transform.GetParameters()
    temp = optimizer.GetValue()
    print "M: %f   P: %f %f " % (temp,
                        currentParameter.GetElement(0),
                        currentParameter.GetElement(1))
    observerData.append(temp)





iterationCommand = itk.PyCommand.New()
iterationCommand.SetCommandCallable(iterationUpdate)
optimizer.AddObserver(itk.IterationEvent(), iterationCommand)

#Start registration
registration.Update()

# Get the final parameters of the transformation
#
finalParameters = registration.GetLastTransformParameters()

print "Final Registration Parameters "
print "Translation X =  %f" % (finalParameters.GetElement(0),)
print "Translation Y =  %f" % (finalParameters.GetElement(1),)

# Resample the moving image to 2D 8bit format
resampler = itk.ResampleImageFilter.IF2IF2.New()
resampler.SetTransform(transform)
resampler.SetInput(movingImage)

region = fixedImage.GetLargestPossibleRegion()

resampler.SetSize(region.GetSize())

resampler.SetOutputSpacing(fixedImage.GetSpacing())
resampler.SetOutputOrigin(fixedImage.GetOrigin())
resampler.SetOutputDirection(fixedImage.GetDirection())
resampler.SetDefaultPixelValue(100)

outputCast = itk.RescaleIntensityImageFilter.IF2IUC2.New()
outputCast.SetOutputMinimum(0)
outputCast.SetOutputMaximum(255)
outputCast.SetInput(resampler.GetOutput())

#  Write the resampled image#
writer = itk.ImageFileWriter.IUC2.New()
writer.SetFileName(sys.argv[3])
writer.SetInput(outputCast.GetOutput())
writer.Update()

# Plot the registration progress.
# This would make more sense inside the observer
# function
plt.plot(observerData)
plt.ylabel("Optimizer value")
plt.show()