from setuptools import setup, find_packages, Extension
import numpy

setup(
    name='supertomo',
    version='0.1',
    packages=find_packages(),
    install_requires=['numpy', 'scipy'],
    entry_points={
        'console_scripts': [
            'supertomo.main = supertomo.bin.main:main',
        ]
    },
    platforms=["any"],
    url='https://bitbucket.org/sakoho81/supertomo',
    license='BSD',
    author='Sami Koho',
    author_email='sami.koho@gmail.com',
    description='supertomo software was created for Tomographic reconstruction '
                'of STED super-resolution microscopy images.',
    ext_modules=[
        # From Microscope sub-module
        Extension(
            'supertomo.microscope.ops_ext',
            ['supertomo/microscope/src/ops_ext.c'],
            include_dirs=[numpy.get_include()]
            ),
        # From IO sub-module
        Extension(
            'supertomo.io._tifffile',
            ['supertomo/io/src/tifffile.c'],
            include_dirs=[numpy.get_include()]),
        # From Ops sub-module
        Extension(
            'supertomo.ops.regress_ext',
            ['supertomo/ops/src/regress_ext.c'],
            include_dirs=[numpy.get_include()])
    ]
)
