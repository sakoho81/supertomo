*Update 2019 Sami: Please take a look at the [Microscope Image Processing Library (MIPLIB)](https://github.com/sakoho81/miplib). I am not going to update the SuperTomo package anymore.*


# SuperTomo  
## A Python package for tomogarphic (STED) microscopy image reconstruction
***


*SuperTomo* is a software developed for tomographic (multiview) STED microscopy image registration and fusion. It is a fork of  [IOCBIO Microscope](https://code.google.com/p/iocbio/wiki/IOCBioMicroscope) Python image deconvolution library, and it can be used as a Python software library (package) or as a standalone application.

The *SuperTomo* software was developed at Laboratory of Biophysics (University of Turku, Finland). It is distributed under [BSD license](https://bitbucket.org/sakoho81/supertomo/wiki/License).

### How does it work?

The image registration functionality in *SuperTomo* was based on the [Insight Toolkit (ITK)](http://itk.org/). Currently 2D/3D rigid registration, with a variety of registration metrics has been implemented - if necessary, the registration functionality can be easily extended.

There are currently four fusion algorithms implemented in *SuperTomo*, all based on iterative maximum likelihood estimation. Please see ...ref... for details. The fusion functionality is an extension of IOCBIO Microscope Richardson-Lucy image deconvolution implementation.



*SuperTomo* uses a simple command-line interface, which was preferred over a graphical one, because it allows easy modification of reconstruction settings as well as using simple *bash* scripts to automate processing tasks. A GUI is available in *IOCBIO Microscope*, but it is by default disabled in *SuperTomo*. This behavior can be changed by modifying the *iocbio.supertomo* (main program) script. Please see the *SuperTomo* Wiki for usage examples.

### How do I get it set up? ###

The following dependencies must be installed before using *SuperTomo*: *Python, WX Python, Numpy, Scipy, libtiff, pyfftw & Matplotlib*. On Ubuntu linux you can accomplish this by running the following command in a terminal window:

```
sudo apt-get install python-dev python-numpy python-scipy \
python-wxgtk2.8 python-matplotlib libtiff4-dev libfftw3-dev gfortran
```

In addition you need the Insight Toolkit (ITK) library, which you have to compile from source, as there are no ready binaries available for the latest versions. Please see [ITK Wiki](http://www.itk.org/Wiki/ITK/WrapITKInstallLinux) for details.

After installing the dependencies you can install *SuperTomo* by running the setup program, that can be found in /supertomo/SuperTomo (assuming that you cloned the repository to a folder called supertomo). You can run the setup by typing the following in you terminal:

	sudo python setup.py install

I have tested *SuperTomo* to run under OSX and Ubuntu Linux. Mac OSX requires ITK version 4.8 to compile, due to some compatibility issues between GCC and Clang compilers that have only recently been fixed. I have not had the chance to try installation on Windows. I would recommend using Linux, if at all possible.

### Contribution guidelines ###

The *SuperTomo* is distributed under BSD open-source license. You can use the software in any way you like; we would just ask you to aknowledge our work:

Koho, S., Deguchi, T. & Hänninen, P.E. (2015) A software tool for tomographic axial superresolution in STED microscopy. Journal of Microscopy, n/a–n/a.

All kinds of contributions, in forms of additional registration/fusion algorithms, new features, bug fixes, documentation, usage examples etc. are welcome.

### Contacts ###

Sami Koho <sami.koho@gmail.com>
